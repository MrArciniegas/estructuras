/*
              -------Creado por-------
             \(x.x )/ Anarchy \( x.x)/
              ------------------------
 */

//    No te olvides de quitar mis comentarios  \\

package back.facade;

import back.dao.entities.VigaDao;
import back.dto.*;
import java.util.ArrayList;
import back.dao.conexion.Conexion;

public class VigaFacade {
  /**
   * Crea un objeto Viga a partir de sus parámetros y lo guarda en base de datos.
   * Puede recibir NullPointerException desde los métodos del Dao
   * @param id
   * @param nombre
   * @param altura
   * @param base
   * @param cargaMuerta
   * @param cargaViva
   * @param longitudAferente
   * @param cortanteMax
   * @param diametro
   * @param piso
   */
  public static void insert(int id, String nombre, float altura, float base, float cargaMuerta, float cargaViva, float longitudAferente, float cortanteMax, int diametro, Piso piso){
      Viga viga = new Viga();
      viga.setId(id); 
      viga.setNombre(nombre); 
      viga.setAltura(altura); 
      viga.setBase(base); 
      viga.setCargaMuerta(cargaMuerta); 
      viga.setCargaViva(cargaViva); 
      viga.setLongitudAferente(longitudAferente); 
      viga.setCortanteMax(cortanteMax); 
      viga.setDiametro(diametro); 
      viga.setPiso(piso); 
     VigaDao vigaDao = new VigaDao(Conexion.obtener());
     vigaDao.insert(viga);
  }
   /**
   * Selecciona un objeto Viga de la base de datos a partir de su(s) llave(s) primaria(s).
   * Puede recibir NullPointerException desde los métodos del Dao
   * @return El objeto en base de datos o Null
   */
   public static ArrayList<Viga> listAll(){

     VigaDao vigaDao = new VigaDao(Conexion.obtener());
      ArrayList<Viga> result = vigaDao.listAll();
      return result;
   }
  /**
   * Modifica los atributos de un objeto Viga  ya existente en base de datos.
   * Puede recibir NullPointerException desde los métodos del Dao
   * @param nombre
   * @param altura
   * @param base
   * @param cargaMuerta
   * @param cargaViva
   * @param longitudAferente
   * @param cortanteMax
   * @param diametro
   * @param id
   */
  public static void update(String nombre, float altura, float base, float cargaMuerta, float cargaViva, float longitudAferente, float cortanteMax, int diametro, int id){
      Viga viga = new Viga();
      viga.setNombre(nombre); 
      viga.setAltura(altura); 
      viga.setBase(base); 
      viga.setCargaMuerta(cargaMuerta); 
      viga.setCargaViva(cargaViva); 
      viga.setLongitudAferente(longitudAferente); 
      viga.setCortanteMax(cortanteMax); 
      viga.setDiametro(diametro); 
     VigaDao vigaDao = new VigaDao(Conexion.obtener());
     vigaDao.update(viga);
  }
  /**
   * Elimina un objeto Viga de la base de datos a partir de su(s) llave(s) primaria(s).
   * Puede recibir NullPointerException desde los métodos del Dao
   * @param id
   */
  public static void delete(int id){
      Viga viga = new Viga();
      viga.setId(id);
     VigaDao vigaDao = new VigaDao(Conexion.obtener());
     vigaDao.delete(viga);
  }

}
//That`s all folks!
