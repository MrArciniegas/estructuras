/*
              -------Creado por-------
             \(x.x )/ Anarchy \( x.x)/
              ------------------------
 */

//    Yo sólo puedo mostrarte la puerta, tú eres quien la tiene que atravesar.  \\

package back.facade;

import back.dao.entities.PisoDao;
import back.dto.*;
import java.util.ArrayList;
import back.dao.conexion.Conexion;

public class PisoFacade {
  /**
   * Crea un objeto Piso a partir de sus parámetros y lo guarda en base de datos.
   * Puede recibir NullPointerException desde los métodos del Dao
   * @param id
   * @param nombre
   * @param proyecto
   */
  public static void insert(int id, String nombre, int proyecto){
      Piso piso = new Piso();
      piso.setId(id); 
      piso.setNombre(nombre); 
      piso.setProyecto(proyecto); 
     PisoDao pisoDao = new PisoDao(Conexion.obtener());
     pisoDao.insert(piso);
  }
   /**
   * Selecciona un objeto Piso de la base de datos a partir de su(s) llave(s) primaria(s).
   * Puede recibir NullPointerException desde los métodos del Dao
   * @return El objeto en base de datos o Null
   */
   public static ArrayList<Piso> listAll(){

     PisoDao pisoDao = new PisoDao(Conexion.obtener());
      ArrayList<Piso> result = pisoDao.listAll();
      return result;
   }

}
//That`s all folks!
