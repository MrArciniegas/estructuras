/*
              -------Creado por-------
             \(x.x )/ Anarchy \( x.x)/
              ------------------------
 */

//    ¿En serio? ¿Tantos buenos frameworks y estás usando Anarchy?  \\

package back.facade;

import back.dao.entities.ProyectoDao;
import back.dto.*;
import java.util.ArrayList;
import back.dao.conexion.Conexion;

public class ProyectoFacade {
  /**
   * Crea un objeto Proyecto a partir de sus parámetros y lo guarda en base de datos.
   * Puede recibir NullPointerException desde los métodos del Dao
   * @param id
   * @param nombre
   * @param usuario
   */
  public static void insert(int id, String nombre, String usuario){
      Proyecto proyecto = new Proyecto();
      proyecto.setId(id); 
      proyecto.setNombre(nombre); 
      proyecto.setUsuario(usuario); 
     ProyectoDao proyectoDao = new ProyectoDao(Conexion.obtener());
     proyectoDao.insert(proyecto);
  }
   /**
   * Selecciona un objeto Proyecto de la base de datos a partir de su(s) llave(s) primaria(s).
   * Puede recibir NullPointerException desde los métodos del Dao
   * @return El objeto en base de datos o Null
   */
   public static ArrayList<Proyecto> listAll(){

     ProyectoDao proyectoDao = new ProyectoDao(Conexion.obtener());
      ArrayList<Proyecto> result = proyectoDao.listAll();
      return result;
   }
  /**
   * Elimina un objeto Proyecto de la base de datos a partir de su(s) llave(s) primaria(s).
   * Puede recibir NullPointerException desde los métodos del Dao
   * @param id
   */
  public static void delete(int id){
      Proyecto proyecto = new Proyecto();
      proyecto.setId(id);
     ProyectoDao proyectoDao = new ProyectoDao(Conexion.obtener());
     proyectoDao.delete(proyecto);
  }
  /**
   * Modifica los atributos de un objeto Proyecto  ya existente en base de datos.
   * Puede recibir NullPointerException desde los métodos del Dao
   * @param nombre
   * @param id
   */
  public static void update(String nombre, int id){
      Proyecto proyecto = new Proyecto();
      proyecto.setNombre(nombre); 
     ProyectoDao proyectoDao = new ProyectoDao(Conexion.obtener());
     proyectoDao.update(proyecto);
  }
  /**
   * Modifica los atributos de un objeto Proyecto  ya existente en base de datos.
   * Puede recibir NullPointerException desde los métodos del Dao
   * @param nombre
   * @param id
   */
  public static void cambiarNombre(String nombre, int id){
      Proyecto proyecto = new Proyecto();
      proyecto.setNombre(nombre); 
     ProyectoDao proyectoDao = new ProyectoDao(Conexion.obtener());
     proyectoDao.cambiarNombre(proyecto);
  }
   /**
   * Selecciona un objeto Proyecto de la base de datos a partir de su(s) llave(s) primaria(s).
   * Puede recibir NullPointerException desde los métodos del Dao
   * @param usuario
   * @return El objeto en base de datos o Null
   */
   public static ArrayList<Proyecto> listarPorUser(String usuario){

      Proyecto proyecto = new Proyecto();
         proyecto.setUsuario(usuario); 

     ProyectoDao proyectoDao = new ProyectoDao(Conexion.obtener());
      ArrayList<Proyecto> result = proyectoDao.listarPorUser(proyecto);
      return result;
   }
   /**
   * Selecciona un objeto Proyecto de la base de datos a partir de su(s) llave(s) primaria(s).
   * Puede recibir NullPointerException desde los métodos del Dao
   * @param id
   * @return El objeto en base de datos o Null
   */
   public static ArrayList<Seccion> getProyecto(int id){

      Proyecto proyecto = new Proyecto();
         proyecto.setId(id); 

     ProyectoDao proyectoDao = new ProyectoDao(Conexion.obtener());
      ArrayList<Seccion> result = proyectoDao.getProyecto(proyecto);
      return result;
   }

}
//That`s all folks!
