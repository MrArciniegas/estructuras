/*
              -------Creado por-------
             \(x.x )/ Anarchy \( x.x)/
              ------------------------
 */

//    Más delgado  \\

package back.facade;

import back.dao.entities.SeccionDao;
import back.dto.*;
import java.util.ArrayList;
import back.dao.conexion.Conexion;

public class SeccionFacade {
  /**
   * Crea un objeto Seccion a partir de sus parámetros y lo guarda en base de datos.
   * Puede recibir NullPointerException desde los métodos del Dao
   * @param viga
   * @param punto
   * @param punto
   */
  public static void insert(Viga viga, Punto punto, Punto punto){
      Seccion seccion = new Seccion();
      seccion.setViga(viga); 
      seccion.setPunto(punto); 
      seccion.setPunto(punto); 
     SeccionDao seccionDao = new SeccionDao(Conexion.obtener());
     seccionDao.insert(seccion);
  }
   /**
   * Selecciona un objeto Seccion de la base de datos a partir de su(s) llave(s) primaria(s).
   * Puede recibir NullPointerException desde los métodos del Dao
   * @return El objeto en base de datos o Null
   */
   public static ArrayList<Seccion> listAll(){

     SeccionDao seccionDao = new SeccionDao(Conexion.obtener());
      ArrayList<Seccion> result = seccionDao.listAll();
      return result;
   }
  /**
   * Modifica los atributos de un objeto Seccion  ya existente en base de datos.
   * Puede recibir NullPointerException desde los métodos del Dao
   * @param punto
   * @param punto
   * @param viga
   */
  public static void update(Punto punto, Punto punto, Viga viga){
      Seccion seccion = new Seccion();
     SeccionDao seccionDao = new SeccionDao(Conexion.obtener());
     seccionDao.update(seccion);
  }
  /**
   * Elimina un objeto Seccion de la base de datos a partir de su(s) llave(s) primaria(s).
   * Puede recibir NullPointerException desde los métodos del Dao
   * @param viga
   * @param punto
   * @param punto
   */
  public static void delete(Viga viga, Punto punto, Punto punto){
      Seccion seccion = new Seccion();
      seccion.setViga(viga);
      seccion.setPunto(punto);
      seccion.setPunto(punto);
     SeccionDao seccionDao = new SeccionDao(Conexion.obtener());
     seccionDao.delete(seccion);
  }

}
//That`s all folks!
