/*
              -------Creado por-------
             \(x.x )/ Anarchy \( x.x)/
              ------------------------
 */

//    Ella existió sólo en un sueño. Él es un poema que el poeta nunca escribió.  \\

package back.facade;

import back.dao.entities.UsuarioDao;
import back.dto.*;
import java.util.ArrayList;
import back.dao.conexion.Conexion;

public class UsuarioFacade {
  /**
   * Crea un objeto Usuario a partir de sus parámetros y lo guarda en base de datos.
   * Puede recibir NullPointerException desde los métodos del Dao
   * @param user
   * @param password
   */
  public static void insert(String user, String password){
      Usuario usuario = new Usuario();
      usuario.setUser(user); 
      usuario.setPassword(password); 
     UsuarioDao usuarioDao = new UsuarioDao(Conexion.obtener());
     usuarioDao.insert(usuario);
  }
   /**
   * Selecciona un objeto Usuario de la base de datos a partir de su(s) llave(s) primaria(s).
   * Puede recibir NullPointerException desde los métodos del Dao
   * @return El objeto en base de datos o Null
   */
   public static ArrayList<Usuario> listAll(){

     UsuarioDao usuarioDao = new UsuarioDao(Conexion.obtener());
      ArrayList<Usuario> result = usuarioDao.listAll();
      return result;
   }

}
//That`s all folks!
