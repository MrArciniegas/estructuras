/*
              -------Creado por-------
             \(x.x )/ Anarchy \( x.x)/
              ------------------------
 */

//    ¿Cuantas frases como esta crees que puedo escribir?  \\

package back.facade;

import back.dao.entities.PuntoDao;
import back.dto.*;
import java.util.ArrayList;
import back.dao.conexion.Conexion;

public class PuntoFacade {
  /**
   * Crea un objeto Punto a partir de sus parámetros y lo guarda en base de datos.
   * Puede recibir NullPointerException desde los métodos del Dao
   * @param id
   * @param station
   * @param mu
   * @param cuantia
   * @param cantidad
   * @param momentoProbable
   * @param viga
   */
  public static void insert(int id, float station, float mu, float cuantia, int cantidad, float momentoProbable, Viga viga){
      Punto punto = new Punto();
      punto.setId(id); 
      punto.setStation(station); 
      punto.setMu(mu); 
      punto.setCuantia(cuantia); 
      punto.setCantidad(cantidad); 
      punto.setMomentoProbable(momentoProbable); 
      punto.setViga(viga); 
     PuntoDao puntoDao = new PuntoDao(Conexion.obtener());
     puntoDao.insert(punto);
  }
   /**
   * Selecciona un objeto Punto de la base de datos a partir de su(s) llave(s) primaria(s).
   * Puede recibir NullPointerException desde los métodos del Dao
   * @return El objeto en base de datos o Null
   */
   public static ArrayList<Punto> listAll(){

     PuntoDao puntoDao = new PuntoDao(Conexion.obtener());
      ArrayList<Punto> result = puntoDao.listAll();
      return result;
   }
  /**
   * Modifica los atributos de un objeto Punto  ya existente en base de datos.
   * Puede recibir NullPointerException desde los métodos del Dao
   * @param mu
   * @param cuantia
   * @param cantidad
   * @param momentoProbable
   * @param id
   */
  public static void update(float mu, float cuantia, int cantidad, float momentoProbable, int id){
      Punto punto = new Punto();
      punto.setMu(mu); 
      punto.setCuantia(cuantia); 
      punto.setCantidad(cantidad); 
      punto.setMomentoProbable(momentoProbable); 
     PuntoDao puntoDao = new PuntoDao(Conexion.obtener());
     puntoDao.update(punto);
  }
  /**
   * Elimina un objeto Punto de la base de datos a partir de su(s) llave(s) primaria(s).
   * Puede recibir NullPointerException desde los métodos del Dao
   * @param id
   */
  public static void delete(int id){
      Punto punto = new Punto();
      punto.setId(id);
     PuntoDao puntoDao = new PuntoDao(Conexion.obtener());
     puntoDao.delete(punto);
  }

}
//That`s all folks!
