/*
              -------Creado por-------
             \(x.x )/ Anarchy \( x.x)/
              ------------------------
 */

//    ¡Oh! (°o° ) ¡es Fredy Arciniegas, el intelectualoide millonario!  \\

package back.dto;

public class Usuario {

  private String user;
  private String password;

    /**
     * Devuelve el valor correspondiente a user
     * @return user
     */
  public String getUser(){
      return this.user;
  }
    /**
     * Modifica el valor correspondiente a user
     * @param user
     */
  public void setUser(String user){
      this.user = user;
  }
    /**
     * Devuelve el valor correspondiente a password
     * @return password
     */
  public String getPassword(){
      return this.password;
  }
    /**
     * Modifica el valor correspondiente a password
     * @param password
     */
  public void setPassword(String password){
      this.password = password;
  }

}
//That`s all folks!
