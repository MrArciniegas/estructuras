/*
              -------Creado por-------
             \(x.x )/ Anarchy \( x.x)/
              ------------------------
 */

//    ¡Teníamos las herramientas! ¡Teníamos el talento! Es hora de una cerveza.  \\

package back.dto;

public class Seccion {

  private Viga viga;
  private Punto punto;
  private Punto punto;

    /**
     * Devuelve el valor correspondiente a viga
     * @return viga
     */
  public Viga getViga(){
      return this.viga;
  }
    /**
     * Modifica el valor correspondiente a viga
     * @param viga
     */
  public void setViga(Viga viga){
      this.viga = viga;
  }
    /**
     * Devuelve el valor correspondiente a inicio
     * @return inicio
     */
  public Punto getPunto(){
      return this.punto;
  }
    /**
     * Modifica el valor correspondiente a inicio
     * @param inicio
     */
  public void setPunto(Punto punto){
      this.punto = punto;
  }
    /**
     * Devuelve el valor correspondiente a fin
     * @return fin
     */
  public Punto getPunto(){
      return this.punto;
  }
    /**
     * Modifica el valor correspondiente a fin
     * @param fin
     */
  public void setPunto(Punto punto){
      this.punto = punto;
  }

}
//That`s all folks!
