/*
              -------Creado por-------
             \(x.x )/ Anarchy \( x.x)/
              ------------------------
 */

//    Traigo una pizza para ¿y se la creyó?  \\

package back.dto;

public class Piso {

  private int id;
  private String nombre;
  private int proyecto;

    /**
     * Devuelve el valor correspondiente a id
     * @return id
     */
  public int getId(){
      return this.id;
  }
    /**
     * Modifica el valor correspondiente a id
     * @param id
     */
  public void setId(int id){
      this.id = id;
  }
    /**
     * Devuelve el valor correspondiente a nombre
     * @return nombre
     */
  public String getNombre(){
      return this.nombre;
  }
    /**
     * Modifica el valor correspondiente a nombre
     * @param nombre
     */
  public void setNombre(String nombre){
      this.nombre = nombre;
  }
    /**
     * Devuelve el valor correspondiente a proyecto
     * @return proyecto
     */
  public int getProyecto(){
      return this.proyecto;
  }
    /**
     * Modifica el valor correspondiente a proyecto
     * @param proyecto
     */
  public void setProyecto(int proyecto){
      this.proyecto = proyecto;
  }

}
//That`s all folks!
