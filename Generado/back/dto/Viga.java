/*
              -------Creado por-------
             \(x.x )/ Anarchy \( x.x)/
              ------------------------
 */

//    Algún día programaremos con la mente...  \\

package back.dto;

public class Viga {

  private int id;
  private String nombre;
  private float altura;
  private float base;
  private float cargaMuerta;
  private float cargaViva;
  private float longitudAferente;
  private float cortanteMax;
  private int diametro;
  private Piso piso;

    /**
     * Devuelve el valor correspondiente a id
     * @return id
     */
  public int getId(){
      return this.id;
  }
    /**
     * Modifica el valor correspondiente a id
     * @param id
     */
  public void setId(int id){
      this.id = id;
  }
    /**
     * Devuelve el valor correspondiente a nombre
     * @return nombre
     */
  public String getNombre(){
      return this.nombre;
  }
    /**
     * Modifica el valor correspondiente a nombre
     * @param nombre
     */
  public void setNombre(String nombre){
      this.nombre = nombre;
  }
    /**
     * Devuelve el valor correspondiente a altura
     * @return altura
     */
  public float getAltura(){
      return this.altura;
  }
    /**
     * Modifica el valor correspondiente a altura
     * @param altura
     */
  public void setAltura(float altura){
      this.altura = altura;
  }
    /**
     * Devuelve el valor correspondiente a base
     * @return base
     */
  public float getBase(){
      return this.base;
  }
    /**
     * Modifica el valor correspondiente a base
     * @param base
     */
  public void setBase(float base){
      this.base = base;
  }
    /**
     * Devuelve el valor correspondiente a cargaMuerta
     * @return cargaMuerta
     */
  public float getCargaMuerta(){
      return this.cargaMuerta;
  }
    /**
     * Modifica el valor correspondiente a cargaMuerta
     * @param cargaMuerta
     */
  public void setCargaMuerta(float cargaMuerta){
      this.cargaMuerta = cargaMuerta;
  }
    /**
     * Devuelve el valor correspondiente a cargaViva
     * @return cargaViva
     */
  public float getCargaViva(){
      return this.cargaViva;
  }
    /**
     * Modifica el valor correspondiente a cargaViva
     * @param cargaViva
     */
  public void setCargaViva(float cargaViva){
      this.cargaViva = cargaViva;
  }
    /**
     * Devuelve el valor correspondiente a longitudAferente
     * @return longitudAferente
     */
  public float getLongitudAferente(){
      return this.longitudAferente;
  }
    /**
     * Modifica el valor correspondiente a longitudAferente
     * @param longitudAferente
     */
  public void setLongitudAferente(float longitudAferente){
      this.longitudAferente = longitudAferente;
  }
    /**
     * Devuelve el valor correspondiente a cortanteMax
     * @return cortanteMax
     */
  public float getCortanteMax(){
      return this.cortanteMax;
  }
    /**
     * Modifica el valor correspondiente a cortanteMax
     * @param cortanteMax
     */
  public void setCortanteMax(float cortanteMax){
      this.cortanteMax = cortanteMax;
  }
    /**
     * Devuelve el valor correspondiente a diametro
     * @return diametro
     */
  public int getDiametro(){
      return this.diametro;
  }
    /**
     * Modifica el valor correspondiente a diametro
     * @param diametro
     */
  public void setDiametro(int diametro){
      this.diametro = diametro;
  }
    /**
     * Devuelve el valor correspondiente a piso
     * @return piso
     */
  public Piso getPiso(){
      return this.piso;
  }
    /**
     * Modifica el valor correspondiente a piso
     * @param piso
     */
  public void setPiso(Piso piso){
      this.piso = piso;
  }

}
//That`s all folks!
