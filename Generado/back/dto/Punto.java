/*
              -------Creado por-------
             \(x.x )/ Anarchy \( x.x)/
              ------------------------
 */

//    Yo a tu edad hacía calculadoras en visualBasic  \\

package back.dto;

public class Punto {

  private int id;
  private float station;
  private float mu;
  private float cuantia;
  private int cantidad;
  private float momentoProbable;
  private Viga viga;

    /**
     * Devuelve el valor correspondiente a id
     * @return id
     */
  public int getId(){
      return this.id;
  }
    /**
     * Modifica el valor correspondiente a id
     * @param id
     */
  public void setId(int id){
      this.id = id;
  }
    /**
     * Devuelve el valor correspondiente a station
     * @return station
     */
  public float getStation(){
      return this.station;
  }
    /**
     * Modifica el valor correspondiente a station
     * @param station
     */
  public void setStation(float station){
      this.station = station;
  }
    /**
     * Devuelve el valor correspondiente a mu
     * @return mu
     */
  public float getMu(){
      return this.mu;
  }
    /**
     * Modifica el valor correspondiente a mu
     * @param mu
     */
  public void setMu(float mu){
      this.mu = mu;
  }
    /**
     * Devuelve el valor correspondiente a cuantia
     * @return cuantia
     */
  public float getCuantia(){
      return this.cuantia;
  }
    /**
     * Modifica el valor correspondiente a cuantia
     * @param cuantia
     */
  public void setCuantia(float cuantia){
      this.cuantia = cuantia;
  }
    /**
     * Devuelve el valor correspondiente a cantidad
     * @return cantidad
     */
  public int getCantidad(){
      return this.cantidad;
  }
    /**
     * Modifica el valor correspondiente a cantidad
     * @param cantidad
     */
  public void setCantidad(int cantidad){
      this.cantidad = cantidad;
  }
    /**
     * Devuelve el valor correspondiente a momentoProbable
     * @return momentoProbable
     */
  public float getMomentoProbable(){
      return this.momentoProbable;
  }
    /**
     * Modifica el valor correspondiente a momentoProbable
     * @param momentoProbable
     */
  public void setMomentoProbable(float momentoProbable){
      this.momentoProbable = momentoProbable;
  }
    /**
     * Devuelve el valor correspondiente a viga
     * @return viga
     */
  public Viga getViga(){
      return this.viga;
  }
    /**
     * Modifica el valor correspondiente a viga
     * @param viga
     */
  public void setViga(Viga viga){
      this.viga = viga;
  }

}
//That`s all folks!
