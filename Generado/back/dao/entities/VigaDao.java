/*
              -------Creado por-------
             \(x.x )/ Anarchy \( x.x)/
              ------------------------
 */

//    Für Elise  \\

package back.dao.entities;
import java.sql.*;
import back.dto.*;
import java.util.ArrayList;

public class VigaDao{

   private Connection cn;

    /**
     * Inicializa una única conexión a la base de datos, que se usará para cada consulta.
     */
   public VigaDao(Connection conexion) {
      this.cn =conexion;
   }

    /**
   * Guarda un objeto Viga en la base de datos.
   * @param viga objeto a guardar
   * @return El id generado para la inserción
   * @throws NullPointerException Si los objetos correspondientes a las llaves foraneas son null
   */
   public void insert(Viga viga) throws NullPointerException{
      try {
         PreparedStatement consulta = cn.prepareStatement(
            "INSERT INTO `Viga` (`id`,`nombre`,`altura`,`base`,`cargaMuerta`,`cargaViva`,`longitudAferente`,`cortanteMax`,`diametro`,`piso`)"
            + " VALUES ('?','?','?','?','?','?','?','?','?','?');"
         );
         consulta.setInt(1, viga.getId());
         consulta.setString(2, viga.getNombre());
         consulta.setFloat(3, viga.getAltura());
         consulta.setFloat(4, viga.getBase());
         consulta.setFloat(5, viga.getCargaMuerta());
         consulta.setFloat(6, viga.getCargaViva());
         consulta.setFloat(7, viga.getLongitudAferente());
         consulta.setFloat(8, viga.getCortanteMax());
         consulta.setInt(9, viga.getDiametro());
         consulta.setInt(10, viga.getPiso().getId());
         consulta.executeUpdate();
         consulta.close();
      } catch (SQLException e) {
         System.out.println(e.getMessage());
      }
   }

   /**
   * Busca un objeto Viga en la base de datos.
   * @param viga objeto con la(s) llave(s) primaria(s) para consultar
   * @return El objeto consultado o null
   * @throws NullPointerException Si los objetos correspondientes a las llaves foraneas son null
   */
   public ArrayList<Viga> listAll() throws NullPointerException{
      ArrayList<Viga> list = new ArrayList();
      try {
         PreparedStatement consulta = cn.prepareStatement(
            "SELECT  Viga.`id`, Viga.`nombre`, Viga.`altura`, Viga.`base`, Viga.`cargaMuerta`, Viga.`cargaViva`, Viga.`longitudAferente`, Viga.`cortanteMax`, Viga.`diametro`, Viga.`piso`"
            + " FROM `Viga` Viga "
            + " WHERE 1;"
         );
         ResultSet res = consulta.executeQuery();
         while(res.next()){

            Viga viga = new Viga();
            viga.setId(res.getInt("id"));
            viga.setNombre(res.getString("nombre"));
            viga.setAltura(res.getFloat("altura"));
            viga.setBase(res.getFloat("base"));
            viga.setCargaMuerta(res.getFloat("cargaMuerta"));
            viga.setCargaViva(res.getFloat("cargaViva"));
            viga.setLongitudAferente(res.getFloat("longitudAferente"));
            viga.setCortanteMax(res.getFloat("cortanteMax"));
            viga.setDiametro(res.getInt("diametro"));
            Piso piso = new Piso();
               piso.setId(res.getInt("piso"));
            viga.setPiso(piso);

            list.add(viga);
         }
         res.close();
         consulta.close();
      } catch (SQLException e) {
         System.out.println(e.getMessage());
         return null;
      }
      return list;
   }
   /**
   * Modifica un objeto Viga en la base de datos.
   * @param viga objeto con la información a modificar
   * @throws NullPointerException Si los objetos correspondientes a las llaves foraneas son null
   */
   public void update(Viga viga) throws NullPointerException{
      try {
         PreparedStatement consulta = cn.prepareStatement(
            "UPDATE `Viga` Viga"
            + "SET `nombre` = '?',`altura` = '?',`base` = '?',`cargaMuerta` = '?',`cargaViva` = '?',`longitudAferente` = '?',`cortanteMax` = '?',`diametro` = '?'"
            + "WHERE  Viga.`id` = '?' ;"
         );
         consulta.setString(1, viga.getNombre());
         consulta.setFloat(2, viga.getAltura());
         consulta.setFloat(3, viga.getBase());
         consulta.setFloat(4, viga.getCargaMuerta());
         consulta.setFloat(5, viga.getCargaViva());
         consulta.setFloat(6, viga.getLongitudAferente());
         consulta.setFloat(7, viga.getCortanteMax());
         consulta.setInt(8, viga.getDiametro());
         consulta.setInt(1, viga.getId());
         consulta.executeUpdate();
         consulta.close();
      } catch (SQLException e) {
         System.out.println(e.getMessage());
      }
   }
    /**
     * Elimina un objeto Viga en la base de datos.
     * @param viga objeto con la(s) llave(s) primaria(s) para consultar
     * @throws NullPointerException Si los objetos correspondientes a las llaves foraneas son null
     */
   public void delete(Viga viga) throws NullPointerException{
      try {
         PreparedStatement consulta = cn.prepareStatement(
            "DELETE FROM `Viga` Viga"
            + "WHERE  Viga.`id` = '?' ;"
         );
         consulta.setInt(1, viga.getId());
         consulta.executeUpdate();
         consulta.close();
      } catch (SQLException e) {
         System.out.println(e.getMessage());
      }
   }

    /**
     * Cierra la conexión actual a la base de datos
     */
   public void close(){
      try {
         cn.close();
      } catch (SQLException e) {
         System.out.println(e.getMessage());
      }
   }


}
//That`s all folks!
