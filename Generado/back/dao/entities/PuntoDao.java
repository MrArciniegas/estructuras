/*
              -------Creado por-------
             \(x.x )/ Anarchy \( x.x)/
              ------------------------
 */

//    Si mi madre entendiera mi código, estaría orgullosa  \\

package back.dao.entities;
import java.sql.*;
import back.dto.*;
import java.util.ArrayList;

public class PuntoDao{

   private Connection cn;

    /**
     * Inicializa una única conexión a la base de datos, que se usará para cada consulta.
     */
   public PuntoDao(Connection conexion) {
      this.cn =conexion;
   }

    /**
   * Guarda un objeto Punto en la base de datos.
   * @param punto objeto a guardar
   * @return El id generado para la inserción
   * @throws NullPointerException Si los objetos correspondientes a las llaves foraneas son null
   */
   public void insert(Punto punto) throws NullPointerException{
      try {
         PreparedStatement consulta = cn.prepareStatement(
            "INSERT INTO `Punto` (`id`,`station`,`mu`,`cuantia`,`cantidad`,`momentoProbable`,`viga`)"
            + " VALUES ('?','?','?','?','?','?','?');"
         );
         consulta.setInt(1, punto.getId());
         consulta.setFloat(2, punto.getStation());
         consulta.setFloat(3, punto.getMu());
         consulta.setFloat(4, punto.getCuantia());
         consulta.setInt(5, punto.getCantidad());
         consulta.setFloat(6, punto.getMomentoProbable());
         consulta.setInt(7, punto.getViga().getId());
         consulta.executeUpdate();
         consulta.close();
      } catch (SQLException e) {
         System.out.println(e.getMessage());
      }
   }

   /**
   * Busca un objeto Punto en la base de datos.
   * @param punto objeto con la(s) llave(s) primaria(s) para consultar
   * @return El objeto consultado o null
   * @throws NullPointerException Si los objetos correspondientes a las llaves foraneas son null
   */
   public ArrayList<Punto> listAll() throws NullPointerException{
      ArrayList<Punto> list = new ArrayList();
      try {
         PreparedStatement consulta = cn.prepareStatement(
            "SELECT  Punto.`id`, Punto.`station`, Punto.`mu`, Punto.`cuantia`, Punto.`cantidad`, Punto.`momentoProbable`, Punto.`viga`"
            + " FROM `Punto` Punto "
            + " WHERE 1;"
         );
         ResultSet res = consulta.executeQuery();
         while(res.next()){

            Punto punto = new Punto();
            punto.setId(res.getInt("id"));
            punto.setStation(res.getFloat("station"));
            punto.setMu(res.getFloat("mu"));
            punto.setCuantia(res.getFloat("cuantia"));
            punto.setCantidad(res.getInt("cantidad"));
            punto.setMomentoProbable(res.getFloat("momentoProbable"));
            Viga viga = new Viga();
               viga.setId(res.getInt("viga"));
            punto.setViga(viga);

            list.add(punto);
         }
         res.close();
         consulta.close();
      } catch (SQLException e) {
         System.out.println(e.getMessage());
         return null;
      }
      return list;
   }
   /**
   * Modifica un objeto Punto en la base de datos.
   * @param punto objeto con la información a modificar
   * @throws NullPointerException Si los objetos correspondientes a las llaves foraneas son null
   */
   public void update(Punto punto) throws NullPointerException{
      try {
         PreparedStatement consulta = cn.prepareStatement(
            "UPDATE `Punto` Punto"
            + "SET `mu` = '?',`cuantia` = '?',`cantidad` = '?',`momentoProbable` = '?'"
            + "WHERE  Punto.`id` = '?' ;"
         );
         consulta.setFloat(1, punto.getMu());
         consulta.setFloat(2, punto.getCuantia());
         consulta.setInt(3, punto.getCantidad());
         consulta.setFloat(4, punto.getMomentoProbable());
         consulta.setInt(1, punto.getId());
         consulta.executeUpdate();
         consulta.close();
      } catch (SQLException e) {
         System.out.println(e.getMessage());
      }
   }
    /**
     * Elimina un objeto Punto en la base de datos.
     * @param punto objeto con la(s) llave(s) primaria(s) para consultar
     * @throws NullPointerException Si los objetos correspondientes a las llaves foraneas son null
     */
   public void delete(Punto punto) throws NullPointerException{
      try {
         PreparedStatement consulta = cn.prepareStatement(
            "DELETE FROM `Punto` Punto"
            + "WHERE  Punto.`id` = '?' ;"
         );
         consulta.setInt(1, punto.getId());
         consulta.executeUpdate();
         consulta.close();
      } catch (SQLException e) {
         System.out.println(e.getMessage());
      }
   }

    /**
     * Cierra la conexión actual a la base de datos
     */
   public void close(){
      try {
         cn.close();
      } catch (SQLException e) {
         System.out.println(e.getMessage());
      }
   }


}
//That`s all folks!
