/*
              -------Creado por-------
             \(x.x )/ Anarchy \( x.x)/
              ------------------------
 */

//    La segunda regla de Anarchy es no hablar de Anarchy  \\

package back.dao.entities;
import java.sql.*;
import back.dto.*;
import java.util.ArrayList;

public class SeccionDao{

   private Connection cn;

    /**
     * Inicializa una única conexión a la base de datos, que se usará para cada consulta.
     */
   public SeccionDao(Connection conexion) {
      this.cn =conexion;
   }

    /**
   * Guarda un objeto Seccion en la base de datos.
   * @param seccion objeto a guardar
   * @return El id generado para la inserción
   * @throws NullPointerException Si los objetos correspondientes a las llaves foraneas son null
   */
   public void insert(Seccion seccion) throws NullPointerException{
      try {
         PreparedStatement consulta = cn.prepareStatement(
            "INSERT INTO `Seccion` (`viga`,`inicio`,`fin`)"
            + " VALUES ('?','?','?');"
         );
         consulta.setInt(1, seccion.getViga().getId());
         consulta.setInt(2, seccion.getPunto().getId());
         consulta.setInt(3, seccion.getPunto().getId());
         consulta.executeUpdate();
         consulta.close();
      } catch (SQLException e) {
         System.out.println(e.getMessage());
      }
   }

   /**
   * Busca un objeto Seccion en la base de datos.
   * @param seccion objeto con la(s) llave(s) primaria(s) para consultar
   * @return El objeto consultado o null
   * @throws NullPointerException Si los objetos correspondientes a las llaves foraneas son null
   */
   public ArrayList<Seccion> listAll() throws NullPointerException{
      ArrayList<Seccion> list = new ArrayList();
      try {
         PreparedStatement consulta = cn.prepareStatement(
            "SELECT  Seccion.`viga`, Seccion.`inicio`, Seccion.`fin`"
            + " FROM `Seccion` Seccion "
            + " WHERE 1;"
         );
         ResultSet res = consulta.executeQuery();
         while(res.next()){

            Seccion seccion = new Seccion();
            Viga viga = new Viga();
               viga.setId(res.getInt("viga"));
            seccion.setViga(viga);
            Punto punto = new Punto();
               punto.setId(res.getInt("inicio"));
            seccion.setPunto(punto);
               punto.setId(res.getInt("fin"));
            seccion.setPunto(punto);

            list.add(seccion);
         }
         res.close();
         consulta.close();
      } catch (SQLException e) {
         System.out.println(e.getMessage());
         return null;
      }
      return list;
   }
   /**
   * Modifica un objeto Seccion en la base de datos.
   * @param seccion objeto con la información a modificar
   * @throws NullPointerException Si los objetos correspondientes a las llaves foraneas son null
   */
   public void update(Seccion seccion) throws NullPointerException{
      try {
         PreparedStatement consulta = cn.prepareStatement(
            "UPDATE `Seccion` Seccion"
            + "SET `inicio` = '?',`fin` = '?'"
            + "WHERE  Seccion.`viga` = '?' ;"
         );
         consulta.setInt(1, seccion.getPunto().getId());
         consulta.setInt(2, seccion.getPunto().getId());
         consulta.setInt(1, seccion.getViga().getId());
         consulta.executeUpdate();
         consulta.close();
      } catch (SQLException e) {
         System.out.println(e.getMessage());
      }
   }
    /**
     * Elimina un objeto Seccion en la base de datos.
     * @param seccion objeto con la(s) llave(s) primaria(s) para consultar
     * @throws NullPointerException Si los objetos correspondientes a las llaves foraneas son null
     */
   public void delete(Seccion seccion) throws NullPointerException{
      try {
         PreparedStatement consulta = cn.prepareStatement(
            "DELETE FROM `Seccion` Seccion"
            + "WHERE  Seccion.`viga` = '?' AND Seccion.`inicio` = '?' AND Seccion.`fin` = '?' ;"
         );
         consulta.setInt(1, seccion.getViga().getId());
         consulta.setInt(2, seccion.getPunto().getId());
         consulta.setInt(3, seccion.getPunto().getId());
         consulta.executeUpdate();
         consulta.close();
      } catch (SQLException e) {
         System.out.println(e.getMessage());
      }
   }

    /**
     * Cierra la conexión actual a la base de datos
     */
   public void close(){
      try {
         cn.close();
      } catch (SQLException e) {
         System.out.println(e.getMessage());
      }
   }


}
//That`s all folks!
