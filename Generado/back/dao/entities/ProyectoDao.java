/*
              -------Creado por-------
             \(x.x )/ Anarchy \( x.x)/
              ------------------------
 */

//    La primera Anarchy que diseñé era perfecta. Una obra de arte, sin fallos. Sublime.  \\

package back.dao.entities;
import java.sql.*;
import back.dto.*;
import java.util.ArrayList;

public class ProyectoDao{

   private Connection cn;

    /**
     * Inicializa una única conexión a la base de datos, que se usará para cada consulta.
     */
   public ProyectoDao(Connection conexion) {
      this.cn =conexion;
   }

    /**
   * Guarda un objeto Proyecto en la base de datos.
   * @param proyecto objeto a guardar
   * @return El id generado para la inserción
   * @throws NullPointerException Si los objetos correspondientes a las llaves foraneas son null
   */
   public void insert(Proyecto proyecto) throws NullPointerException{
      try {
         PreparedStatement consulta = cn.prepareStatement(
            "INSERT INTO `Proyecto` (`id`,`nombre`,`usuario`)"
            + " VALUES ('?','?','?');"
         );
         consulta.setInt(1, proyecto.getId());
         consulta.setString(2, proyecto.getNombre());
         consulta.setString(3, proyecto.getUsuario());
         consulta.executeUpdate();
         consulta.close();
      } catch (SQLException e) {
         System.out.println(e.getMessage());
      }
   }

   /**
   * Busca un objeto Proyecto en la base de datos.
   * @param proyecto objeto con la(s) llave(s) primaria(s) para consultar
   * @return El objeto consultado o null
   * @throws NullPointerException Si los objetos correspondientes a las llaves foraneas son null
   */
   public ArrayList<Proyecto> listAll() throws NullPointerException{
      ArrayList<Proyecto> list = new ArrayList();
      try {
         PreparedStatement consulta = cn.prepareStatement(
            "SELECT  Proyecto.`id`, Proyecto.`nombre`, Proyecto.`usuario`"
            + " FROM `Proyecto` Proyecto "
            + " WHERE 1;"
         );
         ResultSet res = consulta.executeQuery();
         while(res.next()){

            Proyecto proyecto = new Proyecto();
            proyecto.setId(res.getInt("id"));
            proyecto.setNombre(res.getString("nombre"));
            proyecto.setUsuario(res.getString("usuario"));

            list.add(proyecto);
         }
         res.close();
         consulta.close();
      } catch (SQLException e) {
         System.out.println(e.getMessage());
         return null;
      }
      return list;
   }
    /**
     * Elimina un objeto Proyecto en la base de datos.
     * @param proyecto objeto con la(s) llave(s) primaria(s) para consultar
     * @throws NullPointerException Si los objetos correspondientes a las llaves foraneas son null
     */
   public void delete(Proyecto proyecto) throws NullPointerException{
      try {
         PreparedStatement consulta = cn.prepareStatement(
            "DELETE FROM `Proyecto` Proyecto"
            + "WHERE  Proyecto.`id` = '?' ;"
         );
         consulta.setInt(1, proyecto.getId());
         consulta.executeUpdate();
         consulta.close();
      } catch (SQLException e) {
         System.out.println(e.getMessage());
      }
   }
   /**
   * Modifica un objeto Proyecto en la base de datos.
   * @param proyecto objeto con la información a modificar
   * @throws NullPointerException Si los objetos correspondientes a las llaves foraneas son null
   */
   public void update(Proyecto proyecto) throws NullPointerException{
      try {
         PreparedStatement consulta = cn.prepareStatement(
            "UPDATE `Proyecto` Proyecto"
            + "SET `nombre` = '?'"
            + "WHERE  Proyecto.`id` = '?' ;"
         );
         consulta.setString(1, proyecto.getNombre());
         consulta.setInt(1, proyecto.getId());
         consulta.executeUpdate();
         consulta.close();
      } catch (SQLException e) {
         System.out.println(e.getMessage());
      }
   }
   /**
   * Modifica un objeto Proyecto en la base de datos.
   * @param proyecto objeto con la información a modificar
   * @throws NullPointerException Si los objetos correspondientes a las llaves foraneas son null
   */
   public void cambiarNombre(Proyecto proyecto) throws NullPointerException{
      try {
         PreparedStatement consulta = cn.prepareStatement(
            "UPDATE `Proyecto` Proyecto"
            + "SET `nombre` = '?'"
            + "WHERE  Proyecto.`id` = '?' ;"
         );
         consulta.setString(1, proyecto.getNombre());
         consulta.setInt(1, proyecto.getId());
         consulta.executeUpdate();
         consulta.close();
      } catch (SQLException e) {
         System.out.println(e.getMessage());
      }
   }
   /**
   * Busca un objeto Proyecto en la base de datos.
   * @param proyecto objeto con la(s) llave(s) primaria(s) para consultar
   * @return El objeto consultado o null
   * @throws NullPointerException Si los objetos correspondientes a las llaves foraneas son null
   */
   public ArrayList<Proyecto> listarPorUser(Proyecto proyecto) throws NullPointerException{
      ArrayList<Proyecto> list = new ArrayList();
      try {
         PreparedStatement consulta = cn.prepareStatement(
            "SELECT  Proyecto.`id`, Proyecto.`nombre`"
            + " FROM `Proyecto` Proyecto "
            + " WHERE  Proyecto.`usuario` = '?' ;"
         );
         consulta.setString(1, proyecto.getUsuario());
         ResultSet res = consulta.executeQuery();
         while(res.next()){

            Proyecto proyecto = new Proyecto();
            proyecto.setId(res.getInt("id"));
            proyecto.setNombre(res.getString("nombre"));

            list.add(proyecto);
         }
         res.close();
         consulta.close();
      } catch (SQLException e) {
         System.out.println(e.getMessage());
         return null;
      }
      return list;
   }
   /**
   * Busca un objeto Proyecto en la base de datos.
   * @param proyecto objeto con la(s) llave(s) primaria(s) para consultar
   * @return El objeto consultado o null
   * @throws NullPointerException Si los objetos correspondientes a las llaves foraneas son null
   */
   public ArrayList<Seccion> getProyecto(Proyecto proyecto) throws NullPointerException{
      ArrayList<Seccion> list = new ArrayList();
      try {
         PreparedStatement consulta = cn.prepareStatement(
            "SELECT  Proyecto.`id` , Proyecto.`nombre` , Piso.`nombre` AS Piso_nombre , Viga.`nombre` AS Viga_nombre , Viga.`altura` , Viga.`base` , Viga.`cargaMuerta` , Viga.`cargaViva` , Viga.`longitudAferente` , Viga.`cortanteMax` , Viga.`diametro` , Punto.`station` , Punto.`mu` , Punto.`cuantia` , Punto.`cantidad` , Punto.`momentoProbable` , Seccion.`inicio` , Seccion.`fin` , Punto.`station` AS Punto_station , Punto.`station` AS Punto_station "
            + " FROM `Proyecto` Proyecto "
            + " INNER JOIN `Piso` Piso ON Proyecto.`id` = Piso.`proyecto`"
            + " INNER JOIN `Viga` Viga ON Proyecto.`id` = Viga.`piso`"
            + " INNER JOIN `Punto` Punto ON Proyecto.`id` = Punto.`viga`"
            + " INNER JOIN `Seccion` Seccion ON Proyecto.`id` = Seccion.`viga`"
            + " INNER JOIN `Punto` Punto ON Seccion.`inicio` = Punto.`id`"
            + " INNER JOIN `Punto` Punto ON Seccion.`fin` = Punto.`id`"
            + " WHERE  Proyecto.`id` = '?' ;"
         );
         consulta.setInt(1, proyecto.getId());
         ResultSet res = consulta.executeQuery();
         while(res.next()){

            Proyecto proyecto = new Proyecto();
            proyecto.setId(res.getInt("id"));
            proyecto.setNombre(res.getString("nombre"));

            Piso piso = new Piso();
            piso.setNombre(res.getString("Piso_nombre"));

            Viga viga = new Viga();
            viga.setNombre(res.getString("Viga_nombre"));
            viga.setAltura(res.getFloat("altura"));
            viga.setBase(res.getFloat("base"));
            viga.setCargaMuerta(res.getFloat("cargaMuerta"));
            viga.setCargaViva(res.getFloat("cargaViva"));
            viga.setLongitudAferente(res.getFloat("longitudAferente"));
            viga.setCortanteMax(res.getFloat("cortanteMax"));
            viga.setDiametro(res.getInt("diametro"));

            Punto punto = new Punto();
            punto.setStation(res.getFloat("station"));
            punto.setMu(res.getFloat("mu"));
            punto.setCuantia(res.getFloat("cuantia"));
            punto.setCantidad(res.getInt("cantidad"));
            punto.setMomentoProbable(res.getFloat("momentoProbable"));

            Seccion seccion = new Seccion();
               punto.setId(res.getInt("inicio"));
            seccion.setPunto(punto);
               punto.setId(res.getInt("fin"));
            seccion.setPunto(punto);

            punto.setStation(res.getFloat("Punto_station"));

            punto.setStation(res.getFloat("Punto_station"));

            piso.setProyecto(proyecto);
            viga.setProyecto(proyecto);
            punto.setProyecto(proyecto);
            seccion.setProyecto(proyecto);
            seccion.setPunto(punto);
            seccion.setPunto(punto);
            list.add(seccion);
         }
         res.close();
         consulta.close();
      } catch (SQLException e) {
         System.out.println(e.getMessage());
         return null;
      }
      return list;
   }

    /**
     * Cierra la conexión actual a la base de datos
     */
   public void close(){
      try {
         cn.close();
      } catch (SQLException e) {
         System.out.println(e.getMessage());
      }
   }


}
//That`s all folks!
