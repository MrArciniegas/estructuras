/*
              -------Creado por-------
             \(x.x )/ Anarchy \( x.x)/
              ------------------------
 */

//    Yo sólo puedo mostrarte la puerta, tú eres quien la tiene que atravesar.  \\

package back.dao.entities;
import java.sql.*;
import back.dto.*;
import java.util.ArrayList;

public class PisoDao{

   private Connection cn;

    /**
     * Inicializa una única conexión a la base de datos, que se usará para cada consulta.
     */
   public PisoDao(Connection conexion) {
      this.cn =conexion;
   }

    /**
   * Guarda un objeto Piso en la base de datos.
   * @param piso objeto a guardar
   * @return El id generado para la inserción
   * @throws NullPointerException Si los objetos correspondientes a las llaves foraneas son null
   */
   public void insert(Piso piso) throws NullPointerException{
      try {
         PreparedStatement consulta = cn.prepareStatement(
            "INSERT INTO `Piso` (`id`,`nombre`,`proyecto`)"
            + " VALUES ('?','?','?');"
         );
         consulta.setInt(1, piso.getId());
         consulta.setString(2, piso.getNombre());
         consulta.setInt(3, piso.getProyecto());
         consulta.executeUpdate();
         consulta.close();
      } catch (SQLException e) {
         System.out.println(e.getMessage());
      }
   }

   /**
   * Busca un objeto Piso en la base de datos.
   * @param piso objeto con la(s) llave(s) primaria(s) para consultar
   * @return El objeto consultado o null
   * @throws NullPointerException Si los objetos correspondientes a las llaves foraneas son null
   */
   public ArrayList<Piso> listAll() throws NullPointerException{
      ArrayList<Piso> list = new ArrayList();
      try {
         PreparedStatement consulta = cn.prepareStatement(
            "SELECT  Piso.`id`, Piso.`nombre`, Piso.`proyecto`"
            + " FROM `Piso` Piso "
            + " WHERE 1;"
         );
         ResultSet res = consulta.executeQuery();
         while(res.next()){

            Piso piso = new Piso();
            piso.setId(res.getInt("id"));
            piso.setNombre(res.getString("nombre"));
            piso.setProyecto(res.getInt("proyecto"));

            list.add(piso);
         }
         res.close();
         consulta.close();
      } catch (SQLException e) {
         System.out.println(e.getMessage());
         return null;
      }
      return list;
   }

    /**
     * Cierra la conexión actual a la base de datos
     */
   public void close(){
      try {
         cn.close();
      } catch (SQLException e) {
         System.out.println(e.getMessage());
      }
   }


}
//That`s all folks!
