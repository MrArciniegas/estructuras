/*
              -------Creado por-------
             \(x.x )/ Anarchy \( x.x)/
              ------------------------
 */

//    Oh Bartolomeo... me siento como San Agustin de Hipona despues de ser convertido por Ambrosio de Milan.....  \\


package back.dao.conexion;

import back.dao.properties.Properties;
import java.sql.*;

public class Conexion{

   private static Connection cnx = null;
   private static String className="NULL_GESTOR";
   private static String jdbc="NULL_GESTOR";
   
    private static String setParametros(String[] datosDefault) {
        switch (datosDefault[0]){
            case "mysql":{
                className="com.mysql.jdbc.Driver";
                jdbc="jdbc:mysql:";
                break;
            }
            case "postgresql":{
                className="org.postgresql.Driver";
                jdbc="jdbc:postgresql:";
                break;
            }
            case "oracle":{
                className="oracle.jdbc.driver.OracleDriver";
                jdbc="jdbc:oracle:";
                break;
            }
        }
        return datosDefault[1];
    }

    /**
     * Crea una conexión si no se ha establecido antes; en caso contrario, devuelve la ya existente
     * Toma los parámetros de conexión de la clase Properties y usa el driver jdbc para establecer una conexión. 
     * @return Conexión a base de datos del gestor seleccionado
     * @param dbName Nombre de la base de datos que se desea conectar     */
   public static Connection obtener(){
      if (cnx == null) {
         try {
            String dbName = setParametros(Properties.leerDatosDefault());
            Class.forName(className);
            String[] array = Properties.leerProperties(dbName);
            cnx = DriverManager.getConnection(jdbc+"//"+array[0]+"/"+dbName, array[1], array[2]);
         } catch (SQLException ex) {
            System.out.println(ex);
         } catch (ClassNotFoundException ex) {
            System.out.println(ex);
         }
      }
      return cnx;
   }

     /**
     * Cierra la conexión a la base de datos
     * @throws SQLException
     */
   public void cerrar() throws SQLException {
      if (cnx != null) {
         cnx.close();
      }
   }


}
//That`s all folks!
