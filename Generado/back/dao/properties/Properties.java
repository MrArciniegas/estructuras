/*
              -------Creado por-------
             \(x.x )/ Anarchy \( x.x)/
              ------------------------
 */

//    Sólo relájate y deja que alguien más lo haga  \\

package back.dao.properties;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class Properties{

   private static String rutaProperties="Properties.ini";

     /**
     * Busca los parámetros de conexión de la base de datos seleccionada
     * @param bdName Nombre de la base de datos a conectar
     * @return array String[0]=Host, String[1]=username, String[2]=password 
     * @throws SQLException
     */
   public static String[] leerProperties(String bdName){
       File f=new File(rutaProperties);
       String[] array = new String[3];
       Boolean encontrado=false;
       try{
       BufferedReader br=new BufferedReader(new InputStreamReader(new FileInputStream(f.getAbsolutePath()), "UTF8"));
       String linea=br.readLine();
       while(linea!=null){    
         if(linea.startsWith(";") || (linea.trim().isEmpty())){
             linea= br.readLine(); 
             continue;
         }
         if(!encontrado){
             if(linea.contains("["+bdName+"]")){
                 encontrado=true;
             }
         }else{
             if(linea.contains(";")){
                     linea=linea.substring(0,linea.indexOf(";"));
                 }
             if(linea.contains("host=")){
                 linea=linea.trim();
                 linea=linea.replace("host=","");
                 array[0]=linea;
             }else if(linea.contains("username=")){
                 linea=linea.trim();
                 linea=linea.replace("username=","");
                 array[1]=linea;
             }else if(linea.contains("password=")){
                 linea=linea.trim();
                 linea=linea.replace("password=","");
                 array[2]=linea;
             }
         } 
         linea= br.readLine(); 
       }
         } catch (FileNotFoundException ex) {
            System.out.println("Archivo properties.ini no encontrado");
            ex.printStackTrace();
        } catch (IOException ex) {
            System.out.println("Archivo properties.ini no pudo ser leido");
            ex.printStackTrace();
        }
       return array;
   }
     /**
     * Busca los parámetros de conexión de la base de datos seleccionada
     * @return array String[0]=gestor_default, String[1]=database_default 
     * @throws SQLException
     */
   public static String[] leerDatosDefault(){
       File f=new File(rutaProperties);
       String[] array = new String[2];
       Boolean encontrado=false;
       try{
       BufferedReader br=new BufferedReader(new InputStreamReader(new FileInputStream(f.getAbsolutePath()), "UTF8"));
       String linea=br.readLine();
       while(linea!=null){    
         if(linea.startsWith(";") || (linea.trim().isEmpty())){
             linea= br.readLine(); 
             continue;
         }
         if(!encontrado){
             if(linea.contains("[general]")){
                 encontrado=true;
             }
         }else{
             if(linea.contains(";")){
                     linea=linea.substring(0,linea.indexOf(";"));
                 }
             if(linea.contains("gestor_default=")){
                 linea=linea.trim();
                 linea=linea.replace("gestor_default=","");
                 array[0]=linea;
             }else if(linea.contains("database_default=")){
                 linea=linea.trim();
                 linea=linea.replace("database_default=","");
                 array[1]=linea;
             }
         } 
         linea= br.readLine(); 
       }
         } catch (FileNotFoundException ex) {
            System.out.println("Archivo properties.ini no encontrado");
            ex.printStackTrace();
        } catch (IOException ex) {
            System.out.println("Archivo properties.ini no pudo ser leido");
            ex.printStackTrace();
        }
       return array;
   }

}
//That`s all folks!
