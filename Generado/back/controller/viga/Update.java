/*
              -------Creado por-------
             \(x.x )/ Anarchy \( x.x)/
              ------------------------
 */

//    No hay cuchara.  \\

package back.controller.viga;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import java.util.ArrayList;
import back.dto.Viga;
import back.facade.VigaFacade;

@WebServlet(name = "Viga_update", urlPatterns = {"/Viga/update"})
public class Update extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
   protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

       String nombre = request.getParameter("nombre");
       float altura = Float.parseFloat(request.getParameter("altura"));
       float base = Float.parseFloat(request.getParameter("base"));
       float cargaMuerta = Float.parseFloat(request.getParameter("cargaMuerta"));
       float cargaViva = Float.parseFloat(request.getParameter("cargaViva"));
       float longitudAferente = Float.parseFloat(request.getParameter("longitudAferente"));
       float cortanteMax = Float.parseFloat(request.getParameter("cortanteMax"));
       int diametro = Integer.parseInt(request.getParameter("diametro"));
       int id = Integer.parseInt(request.getParameter("id"));

      response.setContentType("text/html; charset=UTF-8");
      response.setCharacterEncoding("UTF-8");
      try (PrintWriter out = response.getWriter()) {
         VigaFacade.update(nombre, altura, base, cargaMuerta, cargaViva, longitudAferente, cortanteMax, diametro, id);

         out.print("true");
      }
   }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>


}
//That`s all folks!
