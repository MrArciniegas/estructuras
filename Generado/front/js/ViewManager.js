/** Variable global que indica el vínculo rápido de acceso al servidor
    No es lo más seguro, pero es bastante rápido :p
*/
var SERVER_IP = "../";


/** Valida los campos requeridos en un formulario
 * Returns flag Devuelve true si el form cuenta con los datos mÃ­nimos requeridos
 */
function validarForm(idForm){
	var form=$('#'+idForm)[0];
	for (var i = 0; i < form.length; i++) {
		var input = form[i];
		if(input.required && input.value==""){
			return false;
		}
	}
	return true;
}

////////// PISO \\\\\\\\\\
function prePiso_insert(idForm){
     //Haga aquí las validaciones necesarias antes de enviar el formulario.
	if(validarForm(idForm)){
 	var formData=$('#'+idForm).serialize();
 	enviar(formData,SERVER_IP+"Piso/insert",postPiso_insert);
 	}else{
 		alert("Debe llenar los campos requeridos");
 	}
}

 function postPiso_insert(result,state){
     //Maneje aquí la respuesta del servidor.
 		if(state=="success"){
                     if(result=="true"){            
 			alert("Piso registrado con éxito");
                     }else{
                        alert("Hubo un errror en la inserción ( u.u)\n"+result);
                     } 		}else{
 			alert("Hubo un errror interno ( u.u)\n"+result);
 		}
}

function prePiso_listAll(container){
     //Solicite información del servidor
     cargaContenido(container,'Piso_listAll.html'); 
     var formData = {};
 	enviar("",SERVER_IP+"Piso/listAll",postPiso_listAll); 
}

 function postPiso_listAll(result,state){
     //Maneje aquí la respuesta del servidor.
     if(state=="success"){
         var json=JSON.parse(result);

            for(var i=0; i < Object.keys(json).length; i++) {   
                var Piso = json[i];
                //----------------- Para una tabla -----------------------
                document.getElementById("Piso_listAll").appendChild(createTR(Piso));
                //-------- Para otras opciones ver htmlBuilder.js ---------
            }
     }else{
         alert("Hubo un errror interno ( u.u)\n"+result);
     }
}

////////// PROYECTO \\\\\\\\\\
function preProyecto_insert(idForm){
     //Haga aquí las validaciones necesarias antes de enviar el formulario.
	if(validarForm(idForm)){
 	var formData=$('#'+idForm).serialize();
 	enviar(formData,SERVER_IP+"Proyecto/insert",postProyecto_insert);
 	}else{
 		alert("Debe llenar los campos requeridos");
 	}
}

 function postProyecto_insert(result,state){
     //Maneje aquí la respuesta del servidor.
 		if(state=="success"){
                     if(result=="true"){            
 			alert("Proyecto registrado con éxito");
                     }else{
                        alert("Hubo un errror en la inserción ( u.u)\n"+result);
                     } 		}else{
 			alert("Hubo un errror interno ( u.u)\n"+result);
 		}
}

function preProyecto_listAll(container){
     //Solicite información del servidor
     cargaContenido(container,'Proyecto_listAll.html'); 
     var formData = {};
 	enviar("",SERVER_IP+"Proyecto/listAll",postProyecto_listAll); 
}

 function postProyecto_listAll(result,state){
     //Maneje aquí la respuesta del servidor.
     if(state=="success"){
         var json=JSON.parse(result);

            for(var i=0; i < Object.keys(json).length; i++) {   
                var Proyecto = json[i];
                //----------------- Para una tabla -----------------------
                document.getElementById("Proyecto_listAll").appendChild(createTR(Proyecto));
                //-------- Para otras opciones ver htmlBuilder.js ---------
            }
     }else{
         alert("Hubo un errror interno ( u.u)\n"+result);
     }
}

function preProyecto_delete(idForm){
     //Haga aquí las validaciones necesarias antes de enviar el formulario.
	if(validarForm(idForm)){
 	var formData=$('#'+idForm).serialize();
 	enviar(formData,SERVER_IP+"Proyecto/delete",postProyecto_delete);
 	}else{
 		alert("Debe llenar los campos requeridos");
 	}
}

 function postProyecto_delete(result,state){
     //Maneje aquí la respuesta del servidor.
 		if(state=="success"){
                     if(result=="true"){            
 			alert("Proyecto eliminado con éxito");
                     }else{
                        alert("Hubo un errror en al eliminar ( u.u)\n"+result);
                     } 		}else{
 			alert("Hubo un errror interno ( u.u)\n"+result);
 		}
}

function preProyecto_update(idForm){
     //Haga aquí las validaciones necesarias antes de enviar el formulario.
	if(validarForm(idForm)){
 	var formData=$('#'+idForm).serialize();
 	enviar(formData,SERVER_IP+"Proyecto/update",postProyecto_update);
 	}else{
 		alert("Debe llenar los campos requeridos");
 	}
}

 function postProyecto_update(result,state){
     //Maneje aquí la respuesta del servidor.
 		if(state=="success"){
                     if(result=="true"){            
 			alert("Proyecto actualizado con éxito");
                     }else{
                        alert("Hubo un errror en la actualización ( u.u)\n"+result);
                     } 		}else{
 			alert("Hubo un errror interno ( u.u)\n"+result);
 		}
}

function preProyecto_cambiarNombre(idForm){
     //Haga aquí las validaciones necesarias antes de enviar el formulario.
	if(validarForm(idForm)){
 	var formData=$('#'+idForm).serialize();
 	enviar(formData,SERVER_IP+"Proyecto/cambiarNombre",postProyecto_cambiarNombre);
 	}else{
 		alert("Debe llenar los campos requeridos");
 	}
}

 function postProyecto_cambiarNombre(result,state){
     //Maneje aquí la respuesta del servidor.
 		if(state=="success"){
                     if(result=="true"){            
 			alert("Proyecto actualizado con éxito");
                     }else{
                        alert("Hubo un errror en la actualización ( u.u)\n"+result);
                     } 		}else{
 			alert("Hubo un errror interno ( u.u)\n"+result);
 		}
}

function preProyecto_listarPorUser(container){
     //Solicite información del servidor
     cargaContenido(container,'Proyecto_listarPorUser.html'); 
     var formData = {};
 	enviar("",SERVER_IP+"Proyecto/listarPorUser",postProyecto_listarPorUser); 
}

 function postProyecto_listarPorUser(result,state){
     //Maneje aquí la respuesta del servidor.
     if(state=="success"){
         var json=JSON.parse(result);

            for(var i=0; i < Object.keys(json).length; i++) {   
                var Proyecto = json[i];
                //----------------- Para una tabla -----------------------
                document.getElementById("Proyecto_listarPorUser").appendChild(createTR(Proyecto));
                //-------- Para otras opciones ver htmlBuilder.js ---------
            }
     }else{
         alert("Hubo un errror interno ( u.u)\n"+result);
     }
}

function preProyecto_getProyecto(container){
     //Solicite información del servidor
     cargaContenido(container,'Proyecto_getProyecto.html'); 
     var formData = {};
 	enviar("",SERVER_IP+"Proyecto/getProyecto",postProyecto_getProyecto); 
}

 function postProyecto_getProyecto(result,state){
     //Maneje aquí la respuesta del servidor.
     if(state=="success"){
         var json=JSON.parse(result);

            for(var i=0; i < Object.keys(json).length; i++) {   
                var Proyecto = json[i];
                //----------------- Para una tabla -----------------------
                document.getElementById("Proyecto_getProyecto").appendChild(createTR(Proyecto));
                //-------- Para otras opciones ver htmlBuilder.js ---------
            }
     }else{
         alert("Hubo un errror interno ( u.u)\n"+result);
     }
}

////////// PUNTO \\\\\\\\\\
function prePunto_insert(idForm){
     //Haga aquí las validaciones necesarias antes de enviar el formulario.
	if(validarForm(idForm)){
 	var formData=$('#'+idForm).serialize();
 	enviar(formData,SERVER_IP+"Punto/insert",postPunto_insert);
 	}else{
 		alert("Debe llenar los campos requeridos");
 	}
}

 function postPunto_insert(result,state){
     //Maneje aquí la respuesta del servidor.
 		if(state=="success"){
                     if(result=="true"){            
 			alert("Punto registrado con éxito");
                     }else{
                        alert("Hubo un errror en la inserción ( u.u)\n"+result);
                     } 		}else{
 			alert("Hubo un errror interno ( u.u)\n"+result);
 		}
}

function prePunto_listAll(container){
     //Solicite información del servidor
     cargaContenido(container,'Punto_listAll.html'); 
     var formData = {};
 	enviar("",SERVER_IP+"Punto/listAll",postPunto_listAll); 
}

 function postPunto_listAll(result,state){
     //Maneje aquí la respuesta del servidor.
     if(state=="success"){
         var json=JSON.parse(result);

            for(var i=0; i < Object.keys(json).length; i++) {   
                var Punto = json[i];
                //----------------- Para una tabla -----------------------
                document.getElementById("Punto_listAll").appendChild(createTR(Punto));
                //-------- Para otras opciones ver htmlBuilder.js ---------
            }
     }else{
         alert("Hubo un errror interno ( u.u)\n"+result);
     }
}

function prePunto_update(idForm){
     //Haga aquí las validaciones necesarias antes de enviar el formulario.
	if(validarForm(idForm)){
 	var formData=$('#'+idForm).serialize();
 	enviar(formData,SERVER_IP+"Punto/update",postPunto_update);
 	}else{
 		alert("Debe llenar los campos requeridos");
 	}
}

 function postPunto_update(result,state){
     //Maneje aquí la respuesta del servidor.
 		if(state=="success"){
                     if(result=="true"){            
 			alert("Punto actualizado con éxito");
                     }else{
                        alert("Hubo un errror en la actualización ( u.u)\n"+result);
                     } 		}else{
 			alert("Hubo un errror interno ( u.u)\n"+result);
 		}
}

function prePunto_delete(idForm){
     //Haga aquí las validaciones necesarias antes de enviar el formulario.
	if(validarForm(idForm)){
 	var formData=$('#'+idForm).serialize();
 	enviar(formData,SERVER_IP+"Punto/delete",postPunto_delete);
 	}else{
 		alert("Debe llenar los campos requeridos");
 	}
}

 function postPunto_delete(result,state){
     //Maneje aquí la respuesta del servidor.
 		if(state=="success"){
                     if(result=="true"){            
 			alert("Punto eliminado con éxito");
                     }else{
                        alert("Hubo un errror en al eliminar ( u.u)\n"+result);
                     } 		}else{
 			alert("Hubo un errror interno ( u.u)\n"+result);
 		}
}

////////// SECCION \\\\\\\\\\
function preSeccion_insert(idForm){
     //Haga aquí las validaciones necesarias antes de enviar el formulario.
	if(validarForm(idForm)){
 	var formData=$('#'+idForm).serialize();
 	enviar(formData,SERVER_IP+"Seccion/insert",postSeccion_insert);
 	}else{
 		alert("Debe llenar los campos requeridos");
 	}
}

 function postSeccion_insert(result,state){
     //Maneje aquí la respuesta del servidor.
 		if(state=="success"){
                     if(result=="true"){            
 			alert("Seccion registrado con éxito");
                     }else{
                        alert("Hubo un errror en la inserción ( u.u)\n"+result);
                     } 		}else{
 			alert("Hubo un errror interno ( u.u)\n"+result);
 		}
}

function preSeccion_listAll(container){
     //Solicite información del servidor
     cargaContenido(container,'Seccion_listAll.html'); 
     var formData = {};
 	enviar("",SERVER_IP+"Seccion/listAll",postSeccion_listAll); 
}

 function postSeccion_listAll(result,state){
     //Maneje aquí la respuesta del servidor.
     if(state=="success"){
         var json=JSON.parse(result);

            for(var i=0; i < Object.keys(json).length; i++) {   
                var Seccion = json[i];
                //----------------- Para una tabla -----------------------
                document.getElementById("Seccion_listAll").appendChild(createTR(Seccion));
                //-------- Para otras opciones ver htmlBuilder.js ---------
            }
     }else{
         alert("Hubo un errror interno ( u.u)\n"+result);
     }
}

function preSeccion_update(idForm){
     //Haga aquí las validaciones necesarias antes de enviar el formulario.
	if(validarForm(idForm)){
 	var formData=$('#'+idForm).serialize();
 	enviar(formData,SERVER_IP+"Seccion/update",postSeccion_update);
 	}else{
 		alert("Debe llenar los campos requeridos");
 	}
}

 function postSeccion_update(result,state){
     //Maneje aquí la respuesta del servidor.
 		if(state=="success"){
                     if(result=="true"){            
 			alert("Seccion actualizado con éxito");
                     }else{
                        alert("Hubo un errror en la actualización ( u.u)\n"+result);
                     } 		}else{
 			alert("Hubo un errror interno ( u.u)\n"+result);
 		}
}

function preSeccion_delete(idForm){
     //Haga aquí las validaciones necesarias antes de enviar el formulario.
	if(validarForm(idForm)){
 	var formData=$('#'+idForm).serialize();
 	enviar(formData,SERVER_IP+"Seccion/delete",postSeccion_delete);
 	}else{
 		alert("Debe llenar los campos requeridos");
 	}
}

 function postSeccion_delete(result,state){
     //Maneje aquí la respuesta del servidor.
 		if(state=="success"){
                     if(result=="true"){            
 			alert("Seccion eliminado con éxito");
                     }else{
                        alert("Hubo un errror en al eliminar ( u.u)\n"+result);
                     } 		}else{
 			alert("Hubo un errror interno ( u.u)\n"+result);
 		}
}

////////// USUARIO \\\\\\\\\\
function preUsuario_insert(idForm){
     //Haga aquí las validaciones necesarias antes de enviar el formulario.
	if(validarForm(idForm)){
 	var formData=$('#'+idForm).serialize();
 	enviar(formData,SERVER_IP+"Usuario/insert",postUsuario_insert);
 	}else{
 		alert("Debe llenar los campos requeridos");
 	}
}

 function postUsuario_insert(result,state){
     //Maneje aquí la respuesta del servidor.
 		if(state=="success"){
                     if(result=="true"){            
 			alert("Usuario registrado con éxito");
                     }else{
                        alert("Hubo un errror en la inserción ( u.u)\n"+result);
                     } 		}else{
 			alert("Hubo un errror interno ( u.u)\n"+result);
 		}
}

function preUsuario_listAll(container){
     //Solicite información del servidor
     cargaContenido(container,'Usuario_listAll.html'); 
     var formData = {};
 	enviar("",SERVER_IP+"Usuario/listAll",postUsuario_listAll); 
}

 function postUsuario_listAll(result,state){
     //Maneje aquí la respuesta del servidor.
     if(state=="success"){
         var json=JSON.parse(result);

            for(var i=0; i < Object.keys(json).length; i++) {   
                var Usuario = json[i];
                //----------------- Para una tabla -----------------------
                document.getElementById("Usuario_listAll").appendChild(createTR(Usuario));
                //-------- Para otras opciones ver htmlBuilder.js ---------
            }
     }else{
         alert("Hubo un errror interno ( u.u)\n"+result);
     }
}

////////// VIGA \\\\\\\\\\
function preViga_insert(idForm){
     //Haga aquí las validaciones necesarias antes de enviar el formulario.
	if(validarForm(idForm)){
 	var formData=$('#'+idForm).serialize();
 	enviar(formData,SERVER_IP+"Viga/insert",postViga_insert);
 	}else{
 		alert("Debe llenar los campos requeridos");
 	}
}

 function postViga_insert(result,state){
     //Maneje aquí la respuesta del servidor.
 		if(state=="success"){
                     if(result=="true"){            
 			alert("Viga registrado con éxito");
                     }else{
                        alert("Hubo un errror en la inserción ( u.u)\n"+result);
                     } 		}else{
 			alert("Hubo un errror interno ( u.u)\n"+result);
 		}
}

function preViga_listAll(container){
     //Solicite información del servidor
     cargaContenido(container,'Viga_listAll.html'); 
     var formData = {};
 	enviar("",SERVER_IP+"Viga/listAll",postViga_listAll); 
}

 function postViga_listAll(result,state){
     //Maneje aquí la respuesta del servidor.
     if(state=="success"){
         var json=JSON.parse(result);

            for(var i=0; i < Object.keys(json).length; i++) {   
                var Viga = json[i];
                //----------------- Para una tabla -----------------------
                document.getElementById("Viga_listAll").appendChild(createTR(Viga));
                //-------- Para otras opciones ver htmlBuilder.js ---------
            }
     }else{
         alert("Hubo un errror interno ( u.u)\n"+result);
     }
}

function preViga_update(idForm){
     //Haga aquí las validaciones necesarias antes de enviar el formulario.
	if(validarForm(idForm)){
 	var formData=$('#'+idForm).serialize();
 	enviar(formData,SERVER_IP+"Viga/update",postViga_update);
 	}else{
 		alert("Debe llenar los campos requeridos");
 	}
}

 function postViga_update(result,state){
     //Maneje aquí la respuesta del servidor.
 		if(state=="success"){
                     if(result=="true"){            
 			alert("Viga actualizado con éxito");
                     }else{
                        alert("Hubo un errror en la actualización ( u.u)\n"+result);
                     } 		}else{
 			alert("Hubo un errror interno ( u.u)\n"+result);
 		}
}

function preViga_delete(idForm){
     //Haga aquí las validaciones necesarias antes de enviar el formulario.
	if(validarForm(idForm)){
 	var formData=$('#'+idForm).serialize();
 	enviar(formData,SERVER_IP+"Viga/delete",postViga_delete);
 	}else{
 		alert("Debe llenar los campos requeridos");
 	}
}

 function postViga_delete(result,state){
     //Maneje aquí la respuesta del servidor.
 		if(state=="success"){
                     if(result=="true"){            
 			alert("Viga eliminado con éxito");
                     }else{
                        alert("Hubo un errror en al eliminar ( u.u)\n"+result);
                     } 		}else{
 			alert("Hubo un errror interno ( u.u)\n"+result);
 		}
}


//That`s all folks!
