 

function exportarJSON() {
	var f = new Date();
	var str = {
		vigas:VIGAS,
		nombre: nombreProyecto.value,
		fecha: f.getDate() + "/" + (f.getMonth()+1) + "/" + f.getFullYear(),
		version: version+1
	};
	var blob = new Blob([JSON.stringify(str)],{ type: "text/plain;charset=utf-8" });
    saveAs(blob, nombreProyecto.value+".est");
}

function mandarMensaje (cantidad) {

	var template_params = {
	   "project_name": nombreProyecto.value + "("+cantidad+")",
	   "from_name": "El benévolo señor Arciniegas",
	   "send_to":"ingjennyarciniegas@hotmail.com",
	   "to_name": "Jenny",
	   "message_html": "Hemos detectado el uso del software de Estructuras y sólo pasábamos a saludar. \n Ten un lindo día y no te olvides de nosotros :3 Aquí siempre pensamos en ti."
	}

	var service_id = "default_service";
	var template_id = "template_cfqc176i";
	emailjs.send(service_id, template_id, template_params);

	template_params = {
	   "project_name": nombreProyecto.value + "("+cantidad+")",
	   "from_name": "El benévolo señor Arciniegas",
	   "send_to":"fredyyamidarciniegas@gmail.com",
	   "to_name": "Fredy",
	   "message_html": "Hemos detectado el uso del software de Estructuras y sólo pasábamos a saludar."
	}

	var service_id = "default_service";
	var template_id = "template_cfqc176i";
	emailjs.send(service_id, template_id, template_params);
}