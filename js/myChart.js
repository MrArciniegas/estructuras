function renderChart(data,modo) {
    var datasets = [];
    var cortantes = [
        {
            label: 'V2',
            data: data[0],
            borderColor: 'rgb(179,149,0)',
            backgroundColor: 'rgba(255, 255, 0, 0.2)',
        },
        {
            label: 'V2a',
            data: data[1],
            borderColor: 'rgb(153,25,0)',
            backgroundColor: 'rgba(255, 85, 0, 0.2)',
        }
    ];
    var momentos = [
        {
            label: 'M3',
            data: data[2],
            borderColor: 'rgba(75, 192, 192, 1)',
            backgroundColor: 'rgba(75, 192, 192, 0.2)',
        },
        {
            label: 'M3a',
            data: data[3],
            borderColor: 'rgb(0,153,0)',
            backgroundColor: 'rgba(0, 250, 0, 0.2)',
        }
    ];

    var canvas = document.getElementById("myChart");
    if(modo=="momentos"){
        datasets=momentos;
        //var canvas = document.getElementById("myChartMomento");
    }else if(modo=="cortantes"){
        datasets=cortantes;
        //var canvas = document.getElementById("myChartCortante");
    }else {
        for (var i = 0; i <2; i++) {
            datasets.push(cortantes[i]);
            datasets.push(momentos[i]);
        };
    }
    var ctx = canvas.getContext('2d');
    if (window.grafica) {
        window.grafica.clear();
        window.grafica.destroy();
    }
    window.grafica = new Chart(ctx, {
        type: 'line',
        data: {
            labels: "labels",
            datasets: datasets
        }, 
        options: {
            scales: {
                xAxes: [{
                    type: 'linear',
                    position: 'bottom'
                }]
            }
        }
    });
}
