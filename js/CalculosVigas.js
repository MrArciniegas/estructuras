
var arregloVarillas = [
  {
    p:"1/4", m:"0.00635"
  },
  {
    p:"3/8", m:"0.009525"
  },
  {
    p:"1/2", m:"0.0127"
  },
  {
    p:"5/8", m:"0.015875"
  },
  {
    p:"3/4", m:"0.01905"
  },
  {
    p:"7/8", m:"0.022225"
  },
  {
    p:"1", m:"0.0254"
  }
];

function cargarDiametrosVarillas (isEstribo) {
  var options = "";
  for (var i = 0; i < arregloVarillas.length; i++) {
    var selected = "";
    if(!isEstribo && arregloVarillas[i].p=="1/2"){
      selected = "selected";
    }else if(isEstribo && arregloVarillas[i].p=="3/8"){
      selected = "selected";
    }
    options += "<option "+selected+" value='"+arregloVarillas[i].m+"'>"+arregloVarillas[i].p+"</option>";
  };
  if(isEstribo){
    diametroEstribo.innerHTML= options;
  }else{
    diametro.innerHTML= options;
  }
}

function varillaPulgadas (m) {
  for (var i = 0; i < arregloVarillas.length; i++) {
    if(arregloVarillas[i].m==m){
      return arregloVarillas[i].p;
    }
  };
}

////////////////////////////////////////////////////////////////////////////////
//                            Listo el pollo
////////////////////////////////////////////////////////////////////////////////


$(document).ready(function(){

  cargarDiametrosVarillas(true);
  cargarDiametrosVarillas(false);

      $("#fileUploader").change(function(evt){
            var selectedFile = evt.target.files[0];
            if(selectedFile.name.substring(selectedFile.name.lastIndexOf("."),selectedFile.name.length) == ".est"){
              
              var reader = new FileReader();
              reader.onload = function(e) { 
                var txt = JSON.parse(e.target.result);
                nombreProyecto.value= txt.nombre;
                version= txt.version;
                VIGAS = txt.vigas;
                //console.log(VIGAS);

                mostrarViga();
                document.getElementById("work_area").style.display = "flex";
                document.getElementById("Inicial").style.display = "none";
              }
              reader.readAsText(selectedFile)
            }else{
              var reader = new FileReader();
              reader.onload = function(event) {
                var data = event.target.result;
                var workbook = XLSX.read(data, {
                    type: 'binary'
                });

                var flag = true;
                workbook.SheetNames.forEach(function(sheetName) {
                  
                    var XL_row_object = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
                    var json_object = JSON.stringify(XL_row_object);

                    if(flag){
                      //=========================
                      nombreProyecto.value="Proyecto sin título";
                      version=0;
                      buscarVigas(XL_row_object);
                      continuarConLosCalculos();
                      document.getElementById("work_area").style.display = "flex";
                      document.getElementById("Inicial").style.display = "none";
                      //=========================
                      flag = false;
                    }
                  })
              };

              reader.onerror = function(event) {
                console.error("File could not be read! Code " + event.target.error.code);
              };

              reader.readAsBinaryString(selectedFile);
            }
      });
});

//==================================================================================================================================
//==================================================================================================================================
//==================================================================================================================================

var VIGAS = [];
var VIGAS_full = [];

function buscarVigas (json) {
  var viga = [];
  var flag = true; 
  //La bandera indica si se está recorriendo la viga por segunda vez. 
  //Inicializa en true para que al entrar, el primer registro la cambie a false.

  for(var i in json) {
    var fila = json[i];
    if(fila["Output Case"]=="Envolvente"){
      if(fila["Station"]==0){
        flag = !flag;
        if(!flag){ 
        //Si no se está recorriendo por segunda vez, entonces es por primera (ooooh) y se guarda la anterior y se empieza una nueva
          if(viga.length > 0){
            VIGAS.push(viga);
            viga = [];
          }
        }
      }
      if(flag){ //Se meten los segundos cosos comparando la station
        viga = addSegundosParametros(viga,fila);
      }else{ //Se registran las primeras filas dentro del objeto
        viga.push(fila);
      }
    }

  }
  if(viga.length > 0){ //Hay que guardar el último que estaba haciendo, ya que no hubo otro 0 para guardarlo.
    VIGAS.push(viga);
  }
}

function addSegundosParametros (viga,fila) {
  for(var i in viga) {
    if(viga[i]["Station"]==fila["Station"] && viga[i]["Element"]==fila["Element"]){
      viga[i]["V2a"]=fila["V2"];
      viga[i]["M3a"]=fila["M3"];
    }
  }
  return viga;
}

//==================================================================================================================================
//==================================================================================================================================
//==================================================================================================================================


function continuarConLosCalculos(){

  VIGAS_full = VIGAS;

  VIGAS = escogerMayores();


  calcularCantidades();
  //console.log(VIGAS);

  mostrarViga();
  calcularCortantesTramo();
  calcularCantidad(VIGAS[0]);
  mostrarViga();
}

function reCalcularViga () {
  
  calcularCantidad(VIGAS[vigaActual]);
  calcularCortantesTramo();
  mostrarViga();
}
//==================================================================================================================================
//==================================================================================================================================
//==================================================================================================================================

function escogerMayores () {
  var VIGASTemp = [];
  var cortantes = [];
    /*
  De las V la mayor de todas
  */
  /*para las m
  0 y último mayor absoluto
  Al encontrar las repetidas, nuevo tramo { 
    maximo absoluto en el borde del tramo (entre las 4 m de los dos repetidos)
    mayor absoluto entre tramos
  }
  */

  for (var i = 0; i < VIGAS.length; i++) {
    var viga = VIGAS[i]; 

    var vigaTemp = {};
    vigaTemp.puntos =[];
    vigaTemp.puntosFull =[];
    vigaTemp.cortanteMax=calculoCortantes(viga);
    vigaTemp.tramos = [];
    var inicioTramo = 0;

    for (var j = 0; j < viga.length; j++) {
      var puntoFull = {};
      puntoFull.Station= viga[j].Station;
      puntoFull.V2 =viga[j].V2; 
      puntoFull.V2a =viga[j].V2a;
      puntoFull.M3 =viga[j].M3;
      puntoFull.M3a =viga[j].M3a;

      vigaTemp.puntosFull.push(puntoFull);

      var fila = {};
      fila.Station = viga[j].Station;
      var momento = viga[j].M3;
      if(j==0 || j == viga.length-1){
        if(Math.abs(momento) < Math.abs(viga[j].M3a) ){
          momento =  viga[j].M3a;
        }
        fila.Mu = momento;
        vigaTemp.puntos.push(fila);
      }
      if(j-1 >=0){
        if(viga[j].Station == viga[j-1].Station){
          vigaTemp = mayoresEntreTramos(inicioTramo , j-1 , j , viga , vigaTemp);
          vigaTemp.tramos.push({inicio: viga[inicioTramo].Station , fin: viga[j-1].Station});
          inicioTramo = j;
        }
      }
    }
    vigaTemp.tramos.push({inicio: viga[inicioTramo].Station , fin: viga[viga.length-1].Station});

    vigaTemp.nombre = viga[0].Beam;
    vigaTemp.piso = viga[0].Story;
    vigaTemp.longitudAferente = 1.075;
    vigaTemp.cargaMuerta = 7.02;
    vigaTemp.cargaViva = 5;
    VIGASTemp.push(vigaTemp);
  }

  return VIGASTemp;
}

function mayoresEntreTramos (inicioTramo, finTramo, inicioSiguiente, viga,vigaTemp) {

  var momentoMayor = 0;
  var tramo = {};
  tramo.Station = viga[inicioTramo+1].Station; //Inicializado en ese porque YOLO
  for (var i = inicioTramo+1; i < finTramo; i++) {
    momentoActual = viga[i].M3;
    if(Math.abs(momentoActual) < Math.abs(viga[i].M3a) ){
      momentoActual =  viga[i].M3a;
    }
    if(Math.abs(momentoActual) > Math.abs(momentoMayor)){
      momentoMayor = momentoActual;
      tramo.Station = viga[i].Station;
    }
  }
  tramo.Mu = momentoMayor;
  vigaTemp.puntos.push(tramo);

  //El mayor de los bordes
  var borde = {};
  borde.Station = viga[finTramo].Station;

  momentoFin = viga[finTramo].M3;
  if(Math.abs(momentoFin) < Math.abs(viga[finTramo].M3a) ){
    momentoFin =  viga[finTramo].M3a;
  }
  momentoInicioNuevo = viga[inicioSiguiente].M3;
  if(Math.abs(momentoInicioNuevo) < Math.abs(viga[inicioSiguiente].M3a) ){
    momentoInicioNuevo =  viga[inicioSiguiente].M3a;
  }
  if(Math.abs(momentoFin) > Math.abs(momentoInicioNuevo)){
    borde.Mu = momentoFin;
  }else{
    borde.Mu = momentoInicioNuevo;
  }
  
  vigaTemp.puntos.push(borde);
  
  return vigaTemp;
}

function calculoCortantes (viga) {
  var mayor = 0;
  for (var i = 0; i < viga.length; i++) {
    v = viga[i].V2;
    if(Math.abs(v) < Math.abs(viga[i].V2a) ){
      v = viga[i].V2a;
    }
    if(Math.abs(v) > Math.abs(mayor)){
      mayor = v;
    }
  }
  return mayor;
}

//==================================================================================================================================
//==================================================================================================================================
//==================================================================================================================================

  /*
    Gráfico de station contra [v,va,m,ma]
  */

  function mandarAGraficar(indice) {
    viga = VIGAS[indice];

    dataV2 = [];
    dataV2a = [];
    dataM3 = [];
    dataM3a = [];
    //Recuerde que las vigas tienen como último coso, un arreglo que define sus tramos. Como este es diferente, no se cuenta.
    for (var i = 0; i < viga.puntosFull.length-1; i++) {
        dataV2.push( 
                { 
                    x: viga.puntosFull[i].Station,
                    y: viga.puntosFull[i].V2
                }
            );
        dataV2a.push( 
                { 
                    x: viga.puntosFull[i].Station,
                    y: viga.puntosFull[i].V2a
                }
            );
        dataM3.push( 
                { 
                    x: viga.puntosFull[i].Station,
                    y: viga.puntosFull[i].M3*-1
                }
            );
        dataM3a.push( 
                { 
                    x: viga.puntosFull[i].Station,
                    y: viga.puntosFull[i].M3a*-1
                }
            );
    };
    renderChart([dataV2,dataV2a,dataM3,dataM3a],"completo");
}


//==================================================================================================================================
//==================================================================================================================================
//==================================================================================================================================
//
//                            SE ACABÓ EL MOMENTO AUTOMÁTICO Y COMIENZA EL DE INTERACCIÓN  
//
//==================================================================================================================================
//==================================================================================================================================
//==================================================================================================================================
  
  var cuantiaMin = 0.003333;
  var cuantiaMax = 0.025000;
  /*if Mcalculado con la cuantía mínima  >= M que tengo en esa fila {usar la mínima y sale}
  else { buscar de pa arriba ¿bisección? para conseguir la cuantía y el momento calculado óptimos}
  */

function calcularCantidades () {
  for (var i = 0; i < VIGAS.length; i++) {
    calcularCantidad(VIGAS[i]);
  }
}

function calcularCantidad (viga) {
  base = document.getElementById("base_input").value;
  d = document.getElementById("altura_input").value - 0.05;
  area= base*d*d;
  
  viga.base=base;
  viga.altura=document.getElementById("altura_input").value;
  viga.diametro = document.getElementById("diametro").value;

  var puntos = viga.puntos;
  for (var j = 0; j < puntos.length; j++) {
    puntos[j].cuantia = calcularCuantia(puntos[j].Mu);
    puntos[j].cantidad = Math.ceil(puntos[j].cuantia * base* d / areaVarilla());

    calcularMomentoProbable(puntos[j]);
  }
}

function calcularMomentoProbable (punto) {
  cuantiaDis= punto.cantidad*areaVarilla()/ (base* d);
    if(cuantiaDis > cuantiaMax){
      alert("cuantia de diseño es mayor a la cuantía máx\n"+as);
    }
  momentoProbable = 1.25*420000*cuantiaDis*(1-0.59*420/21*cuantiaDis)*area;
  punto.momentoProbable = momentoProbable;
}

function calcularCuantia (c) {
 
  var a=4460400*area;
  b=0-(378000*area);
  c = Math.abs(c);
  raiz = Math.sqrt((b*b)-4.0*(a*c));
  cuantia = (0-raiz-b)/(2.0*a);
  cuantiaPos = (raiz-b)/(2.0*a);

  if(cuantia < cuantiaMin){
    cuantia = cuantiaMin;
  }
  return cuantia;
}

function areaVarilla () {
  r= document.getElementById("diametro").value /2 ;
  return 3.141592652 *r*r ;
}

function calcularCortantesTramo() {
  puntos = getPuntosTramo();
  inicio=puntos[0];
  medio=puntos[1];
  fin=puntos[2];

  if(fin == null){
    fin = medio;
  }

  muerta=document.getElementById("cargaMuerta").value;
  VIGAS[vigaActual].cargaMuerta = muerta;
  viva=document.getElementById("cargaViva").value;
  VIGAS[vigaActual].cargaViva = viva;

  longitudAferente = document.getElementById("longitudAferente").value;
  VIGAS[vigaActual].longitudAferente = longitudAferente;
  muertaCalc= muerta * longitudAferente;
  vivaCalc = viva * longitudAferente;
  combinacion = 1.2* muertaCalc + 1.6 * viva * longitudAferente;

  longitud = fin.Station - inicio.Station;
  cortante1a.value = truncar(( inicio.momentoProbable + medio.momentoProbable ) / longitud + (combinacion * longitud / 2));
  cortante1b.value = truncar(( inicio.momentoProbable + medio.momentoProbable ) / longitud - (combinacion * longitud / 2));
  cortante2a.value = truncar(( medio.momentoProbable + fin.momentoProbable ) / longitud - (combinacion * longitud / 2));
  cortante2b.value = truncar(( medio.momentoProbable + fin.momentoProbable ) / longitud + (combinacion * longitud / 2));

  d = altura_input.value - 0.05;
  vc= 0.75*0.17*Math.sqrt(21)*base_input.value*d*1000;
  mayorDeLaViga = VIGAS[vigaActual].cortanteMax;
  params = [];
  params.push(cortante1a.value,cortante1b.value,cortante2a.value,cortante2b.value);
  mayorCortanteCalc = mayorCortanteCalculado(params);
  cortanteAcero = Math.abs(mayorCortanteCalc) - Math.abs(vc);
  if( cortanteAcero > 0 ){
    advertenciaEstribos.style.visibility = "visible";
    var radio = diametroEstribo.value/2;
    areaEstribo=radio*radio*3.14159265;
    separacionEstribos.value = truncar((0.75*2*areaEstribo*420000*d)/cortanteAcero,3);
  }else{
    advertenciaEstribos.style.visibility = "hidden";
    separacionEstribos.value = 0;
  }
}

function mayorCortanteCalculado (params) {
  mayor = 0;
  for (var i = 0; i < params.length; i++) {
    if(Math.abs(params[i]) > Math.abs(mayor) ){
      mayor = params[i];
    }
  }
  return mayor;
}

function reCalcularTramo() {
  actualizarCantidadesTramo();
  puntos = getPuntosTramo();
  for (var i = 0; i < puntos.length; i++) {
      calcularMomentoProbable(puntos[i]);
  };
  mostrarTramo();
}

//==================================================================================================================================
//==================================================================================================================================
//==================================================================================================================================

var inputVigaActual = document.getElementById("vigaActual");

function mostrarViga(){
  vigaActual = inputVigaActual.value - 1;
  Titulo.innerText = "Viga "+ VIGAS[vigaActual].nombre +" - "+ (vigaActual+1) + " de "+VIGAS.length;
  mandarAGraficar(vigaActual);

  viga = VIGAS[vigaActual];

  document.getElementById("diametro").value = viga.diametro;
  document.getElementById("base_input").value = viga.base;
  document.getElementById("altura_input").value = viga.altura;
  document.getElementById("longitudAferente").value = viga.longitudAferente;
  document.getElementById("cargaMuerta").value = viga.cargaMuerta;
  document.getElementById("cargaViva").value = viga.cargaViva;
  document.getElementById("piso").innerText = "Piso "+viga.piso.substring(viga.piso.lastIndexOf("y")+1,viga.piso.length);

  mostrarTramo();
}

function anterior () {
  vigaActual = inputVigaActual.value ;
  if(vigaActual > 1){
    vigaActual--;
  }else{
    vigaActual = VIGAS.length;
  }
  inputVigaActual.value = vigaActual;
  inputTramoActual.value=1;
  mostrarViga();
}

function siguiente () {
  vigaActual = inputVigaActual.value ; 
  if(vigaActual < VIGAS.length){
    vigaActual++;
  }else{
    vigaActual = 1;
  }
  inputVigaActual.value = vigaActual;
  inputTramoActual.value=1;
  mostrarViga();
}


//==================================================================================================================================

var inputTramoActual = document.getElementById("tramoActual");

function mostrarTramo(){
  tramoActual = inputTramoActual.value - 1;
  TituloTramo.innerText = "Sección "+ (tramoActual+1) + " de "+VIGAS[vigaActual].tramos.length;
  tramo = VIGAS[vigaActual].tramos[tramoActual];
  SubtituloTramo.innerText = "Sección "+ tramo.inicio + "m hasta " +tramo.fin+"m";
  calcularCortantesTramo();
}

function getPuntosTramo () {
  puntos = [];
  flag = false;
  for (var i = 0; i < VIGAS[vigaActual].puntos.length; i++) {
    registro = VIGAS[vigaActual].puntos[i];
    if(registro.Station == tramo.inicio){
      cantidadVarillas1a.value = registro.cantidad;
      flag = true;
      puntos[0] = registro;
    }else if(flag){
      cantidadVarillas2a.value = registro.cantidad;
      cantidadVarillas2b.value = registro.cantidad;
      puntos[1] = registro;
      flag = false;
    }else if(registro.Station == tramo.fin){
      cantidadVarillas1b.value = registro.cantidad;
      puntos[2] = registro;
    }
  };
  return puntos;
}

function anteriorTramo () {
  vigaActual = inputVigaActual.value - 1;
  tramoActual = inputTramoActual.value ;
  if(tramoActual > 1){
    tramoActual--;
  }else{
    tramoActual = VIGAS[vigaActual].tramos.length;
  }
  inputTramoActual.value = tramoActual;
  mostrarTramo();
}

function siguienteTramo () {
  tramoActual = inputTramoActual.value ; 
  if(tramoActual < VIGAS[vigaActual].tramos.length){
    tramoActual++;
  }else{
    tramoActual = 1;
  }
  inputTramoActual.value = tramoActual;
  mostrarTramo();
}

function actualizarCantidadesTramo () {
  flag = false;
  for (var i = 0; i < VIGAS[vigaActual].length; i++) {
    registro = VIGAS[vigaActual][i];
    if(registro.Station == tramo.inicio.Station){
      registro.cantidad = cantidadVarillas1a.value;
      flag = true;
      puntos[0] = registro;
    }else if(flag){
      registro.cantidad = cantidadVarillas2a.value;
      cantidadVarillas2b.value = cantidadVarillas2a.value;
      puntos[1] = registro;
      flag = false;
    }else if(registro.Station == tramo.fin.Station){
      registro.cantidad = cantidadVarillas1b.value;
      puntos[2] = registro;
      break;
    }
    VIGAS[vigaActual][i]= registro;
  }
}
/*
///////////////////////////////////////////////////////////////////////////////
                          Durante la impresión
///////////////////////////////////////////////////////////////////////////////
*/
function calcularSeccionMax () {
  var puntoMax;
  var MomProbableMax = 0;
  for (var i = 0; i < VIGAS[vigaActual].puntos.length; i++) {
    registro = VIGAS[vigaActual].puntos[i];
    if(Math.abs(registro.momentoProbable) > Math.abs(MomProbableMax) ){
        MomProbableMax  = registro.momentoProbable;
        puntoMax = registro;
    }
  };
  for (var i = 0; i < VIGAS[vigaActual].tramos.length; i++) {
    var tramo = VIGAS[vigaActual].tramos[i];
    if(tramo.inicio == puntoMax.Station || tramo.fin == puntoMax.Station){
      inputTramoActual.value = i+1;
      mostrarTramo();
      break;
    }
  }
  
}
