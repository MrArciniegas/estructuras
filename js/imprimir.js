function llenarEncabezado() {
    var meses = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
    var f = new Date();
    fecha.innerHTML = f.getDate() + " / " + meses[f.getMonth()] + " / " + f.getFullYear();
}
    
var i = 0;

function exportarPDF(){ 
    //llenarEncabezado();
    var windowContent = '<!DOCTYPE html>';
    windowContent += '<html>'
    windowContent += '<body>'
    //windowContent += '<link rel="stylesheet" href="css/impresion_print.css" media="print">';
    windowContent += '<link rel="stylesheet" href="css/impresion_print.css">';
    windowContent += '<div id="print-header">'+document.getElementById("print-header").innerHTML+'</div>';    
    primeraMitad(windowContent);
} 

function primeraMitad (windowContent) {
    inputVigaActual.value = i+1; //Estoy cambiando de viga como si pulsara el botón next
    inputTramoActual.value = 1; //Miremos a ver, en unas se está lanzando al tramo final y no sé por qué
    mostrarViga();
    
    windowContent +='<table id="tablaContenedora">  <thead><tr><td>    <div class="header-space">&nbsp;</div>  </td></tr></thead>  <tbody><tr><td><div class="content">';
    windowContent += '<h3 class="center">Viga '+VIGAS[vigaActual].nombre+'</h3>'; 
    print_base.innerText = document.getElementById("base_input").value+" m";
    print_altura.innerText = altura_input.value+" m";

    windowContent += print_dimensiones.innerHTML; 

    promesaDeGraficas(windowContent).then(function (result) {
        segundaMitad(result);
    });

}

function segundaMitad (windowContent) {
    print_seccionName.innerText = "Cortante por momentos probables ("+SubtituloTramo.innerText+")";
    print_longitud.innerText = truncar(longitud) +"m";
    print_longAferente.innerText = longitudAferente+"m";
    print_v1a.innerText = truncar(cortante1a.value);
    print_v1b.innerText = truncar(cortante1b.value);
    print_v2a.innerText = truncar(cortante2a.value);
    print_v2b.innerText = truncar(cortante2b.value);
    print_cargaMuerta.innerText= truncar(muerta)+" (KN/m²)";
    print_cargaViva.innerText= truncar(viva)+" (KN/m²)";
    print_cargaMuertaCalc.innerText= truncar(muertaCalc)+" (KN/m)";
    print_cargaVivaCalc.innerText= truncar(vivaCalc)+" (KN/m)";
    print_combinacion.innerText = truncar(combinacion)+" (KN/m)";

    print_Vc.innerText = truncar(vc)+"  KN";
    print_Vc1.innerText = truncar(vc)+"  KN";

    print_Vn.innerText = truncar(mayorDeLaViga)+"  KN";
    print_Vn1.innerText = truncar(mayorCortanteCalc)+"  KN";

    print_Vs.innerText = truncar(Math.abs(mayorDeLaViga)-Math.abs(vc))+"  KN";
    print_Vs1.innerText = truncar(Math.abs(mayorCortanteCalc)-Math.abs(vc))+"  KN";

    var radio = diametroEstribo.value/2;
    areaEstribo=radio*radio*3.14159265;
    
    var dEstribos = truncar((0.75*2*areaEstribo*420000*d)/(mayorDeLaViga-vc));
    if(Math.abs(mayorDeLaViga) - Math.abs(vc) >= 0 && dEstribos1 <= 0.5){
       print_Estribo.innerText = dEstribos+" m";
    }else{
        print_Estribo.innerText = "S min NSR"; 
    }
    
    var dEstribos1 = truncar((0.75*2*areaEstribo*420000*d)/(mayorCortanteCalc-vc));
    if(Math.abs(mayorCortanteCalc) - Math.abs(vc) >= 0 && dEstribos1 <= 0.5){
       print_Estribo1.innerText = dEstribos1+" m";
    }else{
        print_Estribo1.innerText = "S min NSR"; 
    }


    windowContent += print_cortantes.innerHTML; 

    windowContent += '</div>  </td></tr></tbody>  <tfoot><tr><td>    <div class="footer-space">&nbsp;</div>  </td></tr></tfoot></table>';

    calcularSeccionMax();
    i++;
    if(i == VIGAS.length){
        windowContent += '<div id="print-footer" class="center">'+document.getElementById("print-footer").innerHTML+'</div>';
        windowContent += '</body>';
        windowContent += '</html>';
        mandarMensaje(VIGAS.length);
        abrirPaginaImpresion(windowContent);
        i=0;
    }else{
        primeraMitad (windowContent);
    }
}

var delay = 200;

let promesaDeGraficas = function (windowContent) {
    return new Promise(function (resolve, reject) {
        //var src = graficarPrint("momentos");
        var canvas = document.getElementById("myChart");
        graficarPrint("momentos");
        setTimeout(function(){
            windowContent += '<h5>DISEÑO A FLEXIÓN</h5>';
            windowContent += '<img src="' + canvas.toDataURL() + '" class="grafica">';
            graficarPrint("cortantes");
            windowContent += tablaDeDibujoHorizontal();
        },delay);
        setTimeout(function(){
            windowContent += '<h5>DISEÑO A  CORTANTE</h5>';
            windowContent += '<img src="' + canvas.toDataURL() + '" class="grafica">';
            resolve(windowContent);
        },delay * 2);
    });
};

function graficarPrint (modo) {
    renderChart([dataV2,dataV2a,dataM3,dataM3a],modo);
}

function abrirPaginaImpresion (windowContent) {
    printWin = window.open('','','');
    printWin.document.open();
    printWin.document.write(windowContent);
    printWin.document.close();
    printWin.focus();
    printWin.print();   
}


function truncar (n,m=2) {
    var mult = 1;
    for (var i = 0; i < m; i++) {
        mult*=10;
    };

    return Math.trunc(n*mult)/mult;
}

function tablaDeDibujoHorizontal () {
    var tabla = '<table class="NoBorder">';
    var sup = '<tr style="border-top: 3px solid black !important"> <th>Sup</th>';
    var inf = '<tr style="border-bottom: 3px solid black !important"> <th>Inf</th>';
    var dis = '<tr> <th>Dist</th>';
    for (var i = 0; i < VIGAS[vigaActual].puntos.length; i++) {
        var punto = VIGAS[vigaActual].puntos[i];
        var txt = punto.cantidad +" ∅ "+varillaPulgadas(VIGAS[vigaActual].diametro)+"\"<br>Mpr: <br>"+truncar(punto.momentoProbable)+" Kn.m";
        var cs = "<td>";
        var ci = "<td>";
        var cdis = "<td>"+punto.Station+" m</td>";
        if(punto.Mu<0){
            cs+=txt;
            ci+="<br><br>";
        }else{
            cs+="<br><br>";
            ci+=txt;
        }
        cs += "</td>";
        ci += "</td>";
        sup+=cs;
        inf+=ci;
        dis+=cdis;
    };
    sup+="</tr>";
    inf+="</tr>";
    dis+="</tr>";
    tabla+=sup+inf+dis+"</table>";
    return tabla;
}